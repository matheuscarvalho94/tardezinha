export default {
    r100: 'Roboto-Thin',
    r300: 'Roboto-Light',
    r400: 'Roboto-Regular',
    r500: 'Roboto-Medium',
    r700: 'Roboto-Bold',
    r900: 'Roboto-Black',
  };