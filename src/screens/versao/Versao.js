
import React from 'react';
import { StatusBar, Platform, Linking, Text, View, Image, ImageBackground, BackHandler,ToastAndroid } from 'react-native';
import styles from './Versao.styles';
import AppLink from 'react-native-app-link';

import LinearGradient from 'react-native-linear-gradient';

import Buttons from '../../components/Button';
import Loading from '../../components/Loading';

import { general, colors, metrics } from '../../styles'

class VersaoPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder' };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
      ToastAndroid.show('Você precisa atualizar a versão de seu aplicativo', ToastAndroid.SHORT);
      return true;
  }

  openPage(){
    AppLink.openInStore({appName: "Tardezinha", appStoreId: "1204922579", appStoreLocale: "br", playStoreId:"com.tardezinha" }).then(() => {
    })
  }

  render() {
    return (
    <ImageBackground source={require('../../assets/background/1.jpg')} style={[general.structure, { position: 'absolute', top: 0, left: 0, bottom: 0, width: '100%', height: '100%' }]}>
    <StatusBar backgroundColor={colors.pink} barStyle="light-content" />

      <View style={styles.container}>
        <Image source={require('../../assets/icons/version.png')} style={styles.atualizar}/>

        <Text style={styles.h1}>NOVA ATUALIZAÇÃO</Text>

        <Text style={styles.txtDescricao}>É necessário que atualize o aplicativo antes de prosseguir.</Text>

        <View style={[styles.BoxButton]}>
          <View style={[{ backgroundColor: 'rgba(0, 0, 0, 0.4)', width: '100%', height: 50, borderRadius: 30, }]}>
          <Buttons
            title='ATUALIZAR APP'
            width={'100%'}
            gradient={true}
            _onPress={ () => this.openPage() }
            />
          </View>
      </View>
    </View>
  </ImageBackground>
    );
  }
}



export default VersaoPage;
