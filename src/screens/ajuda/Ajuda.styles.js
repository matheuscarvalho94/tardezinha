import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  h1: {
    fontFamily: fonts.r700,
    fontSize: 18,
    color: colors.pink
  },
  p: {
    fontFamily: fonts.r400,
    color: colors.subTxt2,
    fontSize: 16
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default styles;
