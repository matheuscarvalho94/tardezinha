import React from 'react'
import { Text, View, ScrollView, AsyncStorage } from 'react-native'
import Accordion from 'react-native-collapsible/Accordion';
import Icon from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';

import Header from '../../components/Header'
import Buttons from '../../components/Button';
import Loading from '../../components/Loading';

import styles from './Ajuda.styles'
import { colors, metrics, general, fonts } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as FaqActions } from '../../store/ducks/faq';

class AjudaPage extends React.Component {

  state = {
    activeSections: [],
  };

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token');
    this.props.getFaqRequest(token)
  }


  _renderHeader = (section, index, isActive) => {
    return (
      <View style={[styles.header, isActive ?{ marginBottom: 0, paddingBottom: 15,  } :{ marginBottom: 20, paddingBottom: 25, borderBottomWidth: 1, borderColor: colors.divisor} ]}>
        <Text style={[styles.h1, { width: '95%' }]}>{section.Pergunta}</Text>
        <Icon name='angle-down' size={25} color={colors.pink} />
      </View>
    );
  };

  _renderContent = section => {
    return (
      <View style={{ marginTop: 0 }}>
        <HTML html={section.Resposta} tagsStyles={{
          strong: { color: colors.pink, fontSize: 16, marginBottom: 10 },
          p: { color: colors.borderInput, fontSize: 14, fontFamily: fonts.r400, marginBottom: 10 }
        }}  />
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  render() {
    const { navigation } = this.props
    return (
      <View style={[general.structure, { backgroundColor: colors.white }]}>
      <Loading
        loading={this.props.faq.loading}
      />
        <Header
          title='Ajuda'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <View style={[general.container, {padding: metrics.basePadding*2 }]}>
          <ScrollView>
            <Text style={[general.subText3, { marginBottom: 30, padding: 0 }]}>Veja as principais dúvidas</Text>


            <Accordion
              underlayColor={'rgba(0,0,0,0.1)'}
              expandMultiple={true}
              sections={this.props.faq.data}
              activeSections={this.state.activeSections}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
              style={{ marginBottom: 30, paddingBottom: 25, borderBottomWidth: 1, borderColor: colors.divisor }}
            />

          </ScrollView>
        </View>
        <View style={{ position: 'absolute', bottom: metrics.basePadding, right: metrics.basePadding}}>
        <Buttons
          title='CONTATO'
          icon='microphone'
          width={'95%'}
          gradient={true}
          _onPress={() => navigation.navigate("ContatoPage")}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  faq: state.faq,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...FaqActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AjudaPage);

