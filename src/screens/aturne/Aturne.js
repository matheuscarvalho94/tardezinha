import React from 'react'
import { Text, View, Image, ScrollView, AsyncStorage } from 'react-native'
import HTML from 'react-native-render-html';

import Header from '../../components/Header'
import Loading from '../../components/Loading';

import styles from './Aturne.styles'
import { colors, general, metrics, fonts } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as AturneActions } from '../../store/ducks/aturne';


class AturnePage extends React.Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token');
    this.props.getAturneRequest(token);

  }

  render() {

    const {navigation, aturne} = this.props;
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
        <Loading
          loading={this.props.aturne.loading}
        />
        <Header
          title={aturne.data.Titulo}
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <ScrollView>
          <Text style={[general.subText3, { paddingHorizontal: metrics.basePadding*2}]}>{aturne.data.Subtitulo}</Text>
          {
            aturne.data.Imagem
            ?<Image
              source={{uri: aturne.data.Imagem}}
              style={styles.fullimage}
              />
            :null
          }
          {
            this.props.aturne.loading
            ?null
            :<View style={[styles.box, aturne.data.Imagem?{ marignTop: -50} :{marginTop: 0}]}>
              <HTML html={aturne.data.Conteudo}
               tagsStyles={{
                strong: { color: colors.pink, fontSize: 16, marginBottom: 10 },
                p: { color: colors.borderInput, fontSize: 14, fontFamily: fonts.r400, marginBottom: 10 }
              }}
    />
            </View>
          }

        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  aturne: state.aturne,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...AturneActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AturnePage);

