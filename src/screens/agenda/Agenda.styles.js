import { StyleSheet } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  wrapHome: {
    marginTop: 5,
    padding: metrics.basePadding
  }
});

export default styles;
