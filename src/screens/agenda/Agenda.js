import React, { Component } from 'react'
import { RefreshControl, ScrollView, View, AsyncStorage, ActivityIndicator, StatusBar, BackHandler } from 'react-native'
import FlatlistInfiniteScroll from '../../components/FlatlistInfiniteScroll'

import { general, colors } from '../../styles'
import styles from './Agenda.styles'

import Header from '../../components/Header';
import Agenda from '../../components/Agenda';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as EventosActions } from '../../store/ducks/eventos';
const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 100;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};
class AgendaPage extends Component {

  state={
    token: null,
    page: 1,
    refreshing: false,
    quantidade: 1004,
    loading: false
  }

  _onRefresh = async () => {
    this.props.getEventosRequest(this.state)
    this.setState({ refreshing: false })
  }


  handleBackButton = () => {
    this.props.navigation.goBack()
  }

  nextPage = async () => {
    await this.setState({ page: this.state.page + 1 })
  }

  componentDidMount = async () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    let token = await AsyncStorage.getItem('token')
    this.setState({
      token: token,
      quantidade: 12313215
    })
    this.props.getEventosRequest(this.state)
  }

  renderList = () => (
    <FlatlistInfiniteScroll
        itensPorPagina={5}
        page={this.state.page}
        data={this.props.eventos.data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) =>
        <Agenda
          data={item}
          navigation={this.props.navigation}
        />
      }
    />
  );

  render() {
    return (
      <View style={[general.structure, { justifyContent: 'flex-start', backgroundColor: '#fff' }]}>
        <StatusBar backgroundColor={colors.pink} barStyle="light-content" />
        <Header
          title='Agenda'
          background={['#EA5296', '#EF7571', '#F4A03F']}
          photoPerfil={true}
          navigation={this.props.navigation}
          nocadastro={true}/>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }

         onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            this.nextPage();
          }
        }}>
          <View style={styles.wrapHome}>
          {
            this.props.eventos.loading
            ?<ActivityIndicator style={{ position: 'absolute', left: 0, right: 0 }} />
            :null
          }

            {
              this.renderList()
            }
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  eventos: state.eventos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...EventosActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AgendaPage);
