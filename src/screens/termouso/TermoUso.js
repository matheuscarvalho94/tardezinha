import React from 'react';
import { Text, View, Image, ScrollView,Dimensions, StyleSheet } from 'react-native';
import {colors, metrics} from '../../styles';

import Icon from 'react-native-vector-icons/FontAwesome';

import HTML from 'react-native-render-html';
import Loading from '../../components/Loading';

class TermoUso extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder' };
  }

  async componentDidMount() {
  }

  render() {
    const { paginas, navigation } = this.props;
    const { goBack } = navigation;


    return (
      <ScrollView>
        <View style={[styles.container, { backgroundColor: colors.white }]}>
          <Text style={styles.iconTimes} onPress={() => goBack()}>
            <Icon name="times" size={25}/>
          </Text>

          <View style={styles.conteudo}>


            <Text style={styles.txtEsquecisenha}>TERMO DE USO E POLÍTICA DE PRIVACIDADE</Text>

            <Text>Lorem IMpsun impsun Lorem IMpsun impsun Lorem IMpsun impsun Lorem IMpsun impsun Lorem IMpsun impsunLorem IMpsun impsunLorem IMpsun impsunLorem IMpsun impsunLorem IMpsun impsun</Text>


          </View>

        </View>
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 50,
    backgroundColor: '#ffffff'
  },
  conteudo: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#ffffff',
    width: '100%',
    padding: 10
  },
  logo: {
    width: 200,
    height: 114,
    resizeMode: 'contain'
  },
  txtEsquecisenha: {
    color: '#2F2F2F',
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 16,
    width: 200
  },
  txtDescricao: {
    fontSize: 12,
    color: "#2F2F2F",
    width: metrics.defaultWidthPag,
    textAlign: "justify",
    lineHeight: 17
  },
  footer: {
    width: metrics.defaultWidthPag,
    textAlign: "left",
    marginBottom: 10
  },
  iconTimes: {
      color: colors.yellow,
      fontSize: 23,
      position: 'absolute',
      top: 35,
      right: 25,
      zIndex: 1
  },
  iconBack: {
    width: 45
  }
})

export default TermoUso;
