import React, { Component } from 'react'
import { AsyncStorage, View, FlatList, ScrollView, Text } from 'react-native'

import Header from '../../components/Header'
import Voucher from '../../components/Voucher'
import Loading from '../../components/Loading';

import {ENTRIES6} from '../../json/voucher'
import styles from './Voucher.styles'
import { colors, metrics, general } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as VoucherActions } from '../../store/ducks/voucher';

class VoucherPage extends Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token')
    this.props.getVoucherRequest(token);
  }

  postVoucher = (params) => {
    this.props.navigation.push('VoucherDetalhesPage',{
      voucherDetails: params
    })
  }

  renderList(param){
    return(
      <FlatList
        data={this.props.voucher.data.filter(item => item.Expirado === param ?true :false)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) =>
          <Voucher
            data={item}
            postVoucher={() => this.postVoucher(item)}
          />
        }
      />
    )
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
      <Loading
          loading={this.props.voucher.loading}
        />
        <Header
          title='Voucher'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <ScrollView>
        <View style={general.container}>
          <View >
            <Text style={general.subText3}>Disponíveis</Text>
            {
              this.props.voucher.data.find(item => item.Expirado === false)
              ?this.renderList(false)
              :<Text style={[general.subText2, {fontSize: 14, padding: metrics.basePadding, }]}>Nenhum voucher disponível</Text>
            }

            <View style={{ marginTop: 20 }}>
              <Text style={general.subText3}>Utilizados</Text>
              {
                this.props.voucher.data.find(item => item.Expirado === true)
                ?this.renderList(true)
      
      :<Text style={[general.subText2, {fontSize: 14, padding: metrics.basePadding,}]}>Você ainda não utilizou nenhum voucher </Text>
              }
            </View>
          </View>
        </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  voucher: state.voucher,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...VoucherActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(VoucherPage);
