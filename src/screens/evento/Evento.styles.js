import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  fullimage: {
    resizeMode: 'cover',
    height: 240,
    width: '100%'
  },
  box: {
    width: '90%',
    backgroundColor: colors.white,
    borderRadius: 10,
    borderColor: 'rgba(238,238,238, 0.7)',
    borderWidth: 1,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: -50,
    padding: metrics.basePadding*2,
    flexDirection: 'row',
  },
  h2: {
      fontFamily: fonts.r700,
      color: colors.purple,
      fontSize: 18,
      marginBottom: 0,
  },
  date: {
    flexDirection: 'column',
    marginRight: 15
  },
  mes: {
    fontFamily: fonts.r700,
    color: colors.pink,
    fontSize: 22,
  },
  dia: {
    fontFamily: fonts.r700,
    color: colors.pink,
    fontSize: 36,
    marginTop: -10
  },
  txtNumber: {
    color: colors.pink,
    fontSize: 15,
    fontFamily: fonts.r400,
    marginBottom: 10,
  },
  buttonCount: {
    height: 36,
    width: 36,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 2,
    backgroundColor: colors.pink,
    borderRadius: 100,
    marginRight: 5,
    marginTop: -10
  },
  textinput: {
    height: 40,
    paddingVertical: Platform.OS==='android'? 0: 0,
    paddingHorizontal: metrics.basePadding,
    marginBottom: 10,
    marginRight: 10,
    width: '70%',
    backgroundColor: 'transparent',
    borderColor: colors.pink,
    borderWidth: 3,
    borderRadius: 10,
    fontSize: 16,
    color: colors.borderInput,
    fontFamily: fonts.r400,
  },
  txt: {
      fontFamily: fonts.r700,
      color: colors.purple,
      fontSize: 16
  },
  txtBold: {
    fontFamily: fonts.r700,
    marginBottom: 15,
    fontSize: 16,
  },
  containerBox: {
    position: 'relative',
    backgroundColor: colors.white,
    paddingVertical: metrics.basePadding*2,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    width: '100%',
},
h1: {
   fontFamily: fonts.r700,
   color: colors.purple,
   fontSize: 18,
   textAlign: 'center',
   width: '100%'
},
});

export default styles;
