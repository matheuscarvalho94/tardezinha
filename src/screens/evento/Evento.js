import React, { Component } from 'react'
import { Platform, Text, View, ScrollView, Image, AsyncStorage, Alert, TouchableOpacity, Linking, Modal, BackHandler } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import * as RNIap from 'react-native-iap';

//Components
import Header from '../../components/Header';
import Buttons from '../../components/Button';
import ButtonBackground from '../../components/ButtonBackground';
import Box from '../../components/Box';
import Loading from '../../components/Loading';

import styles from './Evento.styles'
import config from '../../config/config';
import { colors, metrics, general, fonts } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as EventosActions } from '../../store/ducks/eventos';
import { Creators as ContribuirActions } from '../../store/ducks/contribuir';

const meses = ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
const getDate = new Date()
const contribuite = 'contribuinte'
const contribuintes = 'contribuintes'

const itemSkus = Platform.select({
  ios: [
    '2',
    '3',
    '4',
    '5',
    '6',
  ],
  android: [
    '2',
    '3',
    '4',
    '5',
    '6',
  ],
});
class EventoPage extends Component {
  constructor(props){
    super(props)
    this.state = {
      Id: null,
      token: null,
      count: 0,
      contribuir: false,
      value: 0,
      page: null,
      diferencaDias: false,
      modalVisible: false,
      purchase: '',
      GoogleSubscriptionId: '',
      GooglePlayId: '',
      GooglePurchaseToken: '',
      AppStoreSubscriptionId: '',
      AppStoreId: '',
      AppStorePurchaseToken: '',
    }
  }

  static navigationOptions = ({navigation}) => ({
    tabBarVisible: false,
    animationEnabled: false
  })

  count(){
   this.state.count==5 ?null :this.setState({ 
      count: this.state.count + 1,
      value: this.state.value + 25.
    })
  }

  uncount(){
    this.state.count==0 ?null :this.setState({ 
      count: this.state.count - 1,
      value: this.state.value - 25.
    })

  }

  buyItem = async () => {
    try {
      if(this.state.count==1){
        if(Platform.OS==='android'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[0].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'android',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }else if(Platform.OS==='ios'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[0].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'ios',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }
      }else if(this.state.count==2){
        if(Platform.OS==='android'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[1].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'android',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }else if(Platform.OS==='ios'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[1].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'ios',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }
      }else if(this.state.count==3){
        if(Platform.OS==='android'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[2].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'android',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }else if(Platform.OS==='ios'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[2].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'ios',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }
      }else if(this.state.count==4){
        if(Platform.OS==='android'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[3].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'android',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }else if(Platform.OS==='ios'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[3].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'ios',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }
      }else if(this.state.count==5){
        if(Platform.OS==='android'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[4].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'android',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }else if(Platform.OS==='ios'){
          try {
            const purchase  = await RNIap.buyProduct(this.state.productList[4].productId);
            console.log(purchase, 'purchasseee')
            this.setState({
              Platform: 'ios',
              ProductId: purchase.transactionId,
              TokenProduct: purchase.purchaseToken,
            })
            this.postContribuir()
          } catch (err) {
            console.log(err.code, err.message, 'ERRO');
          }
        }
      }
    } catch (err) {
      console.log(err.code, err.message);
    }
  }

  componentDidMount = async () => {
    this.setState({
      token: await AsyncStorage.getItem('token'),
      Id: this.props.navigation.state.params.EventoDetails.Id,
    })
    this.pegaDiaMes();
    this.verificardata()
    try {
      const result = await RNIap.initConnection();
      await RNIap.consumeAllItems();
      console.log('result', result);
    } catch (err) {
      console.warn(err.code, err.message);
    }

    try {
      const products = await RNIap.getProducts(itemSkus);
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  }

  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleOpenURL(url) {
    url?Linking.openURL(url):null
  }
  
  handleBackButton = () => {
    // Alert.alert('Vai voltar', 'volta')
    this.props.navigation.goBack()
  }

  verificardata(){
    const { navigation } = this.props;
    var dataJson = navigation.state.params.EventoDetails.DataEvento.split('T')
    var moment1 =  moment(getDate).format("MM/DD/YYYY")
    var moment2 =  moment(dataJson[0]).format("MM/DD/YYYY")

    var dateget1 = new Date(moment1)
    var dateget2 = new Date(moment2)

    var timeDiff = Math.abs(dateget1.getTime() - dateget2.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    if(diffDays<=30){
      this.setState({
        diferencaDias: true
      })
    }else{
      this.setState({
        diferencaDias: false
      })
    }
  }

  onChanged(text){
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < text.length; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
            newText = newText + text[i];
        }
        else {
            // your call back function
            alert("please enter numbers only");
        }
    }
    this.setState({ count: newText });
  }

  postContribuir = () => {
    this.props.getContribuirRequest(this.state)
  }

  pegaDiaMes() {
    const { navigation } = this.props;
    var dataJson = navigation.state.params.EventoDetails.DataEvento.split('-')
    var diasemana = navigation.state.params.EventoDetails.DataExtenso.split(',')
    var diaSemanal = diasemana[0]
    var dia = dataJson[2].split('T');
    var diacerto = dia[0]
    var mes = dataJson[1];
    navigation.state.params.EventoDetails.diasemana = diaSemanal;
    navigation.state.params.EventoDetails.diaFormatado = diacerto;
    navigation.state.params.EventoDetails.mesFormatado = meses[mes - 1].valueOf();
  }

  async componentWillReceiveProps(props){
    const {contribuir} = props
    contribuir.data.Success==true ?this.setState({modalVisible: true}):null

    if(contribuir.error_message=='Desculpe, a data limite para troca do benefício expirou.'){
      contribuir.loading=false
      await Alert.alert(config.nameApp, 'Desculpe, a data limite para troca do benefício expirou.')
      contribuir.error_message=''
    }
  }


  renderContribuir() {
    return(
    <View style={[general.flexColumn, {marginBottom: 30}]}>
      <View style={[styles.box, { marginTop: 0, flexDirection: 'column', marginBottom: 20, }]}>
      <Text style={styles.txtNumber}>NÚMERO DE CONTRIBUIÇÕES</Text>
        <View style={[general.flexRow, { alignItems: 'center' ,
        borderColor: 'rgba(238,238,238, 0.7)',
        borderBottomWidth: 1, paddingBottom: 10}]}>

          <View style={styles.textinput}>
            <Text style={{ fontFamily: fonts.r400, color: colors.subTxt2, paddingTop: 10, }}>{this.state.count} {this.state.count<2 ?contribuite :contribuintes}</Text>
          </View>

          <TouchableOpacity style={styles.buttonCount} onPress={() => this.count() }>
            <Icon name='plus' size={15} color={colors.white} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.buttonCount} onPress={() => this.uncount() }>
            <Icon name='minus' size={15} color={colors.white} />
          </TouchableOpacity>
        </View>

        <View style={[general.flexColumn, { marginTop: 20 }]}>
          <Text style={[styles.txtNumber, { marginBottom: 15 }]}>PREÇO</Text>
          <View style={[general.flexRow, { justifyContent: 'space-between', alignItems: 'center' }]}>
            <Text style={[styles.txtBold, { color: colors.subTxt1 }]}>Valor Unitário</Text>
            <Text style={[styles.txtBold, { color: colors.subTxt1 }]}>R$25,00</Text>
          </View>
          <View style={[general.flexRow, { justifyContent: 'space-between', alignItems: 'center' }]}>
            <Text style={[styles.txtBold, {color: colors.pink}]}>Total</Text>
            {
              this.state.count==0
              ?
              <Text style={[styles.txtBold, {color: colors.pink}]}>R$00,00</Text>
              :
              <Text style={[styles.txtBold, {color: colors.pink}]}>R${this.state.value},00</Text>
            }
          </View>
        </View>
      </View>
      <Buttons
        title='CONTRIBUIR'
        width={'85%'}
        gradient={true}
        _onPress={() => { this.buyItem() }}
        />
    </View>
    )
  }

  renderButtons(){
    return(
    <View style={[general.flexColumn, {paddingHorizontal: metrics.basePadding*2, marginBottom: 30}]}>
    {
      this.props.navigation.state.params.EventoDetails.LinkExterno
      ?<Buttons
        title='COMPRAR INGRESSO'
        width={'100%'}
        gradient={true}
        _onPress={() => this.handleOpenURL(this.props.navigation.state.params.EventoDetails.LinkExterno)}
        />
      :null
    }
    {
      this.state.diferencaDias && this.props.navigation.state.params.EventoDetails.Indisponivel
      ?
        <View>
          <ButtonBackground
            title='FAZER MINHA DOAÇÃO'
            width={'100%'}
            gradient={true}
            _onPress={
              () =>
              this.setState({
                contribuir: true
              })
            }
            />
          <Box
            width={'70%'}
            message='Você pode fazer uma doação ou levar um quilo de alimento não perecível'
          />
        </View>
        :null
      }
    </View>
    )
  }

  componentWillUnmount(){
    this.setState({
      modalVisible: false
    })
  }

  render() {
    const { navigation, contribuir } = this.props;
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
      <Loading
        loading={this.props.contribuir.loading}
      />
        <Header
          title='Evento'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <ScrollView >
          <Text style={[general.subText3, { paddingHorizontal: metrics.basePadding*2 }]}>{navigation.state.params.EventoDetails.Titulo}</Text>
          <Image
            source={{ uri: navigation.state.params.EventoDetails.PathImagem }}
            style={styles.fullimage}
            />
          <View style={styles.box}>
            <View style={[general.flexColumn, { justifyContent: 'center', marginRight: 20 }]}>
              <Text style={styles.mes}>{navigation.state.params.EventoDetails.mesFormatado}</Text>
              <Text style={styles.dia}>{navigation.state.params.EventoDetails.diaFormatado}</Text>
            </View>
            <View style={[general.flexColumn]}>
            <Text style={styles.h2}>{navigation.state.params.EventoDetails.diasemana},{navigation.state.params.EventoDetails.HoraSeparada}</Text>
            <Text style={general.p}>{navigation.state.params.EventoDetails.Chamada}</Text>
            </View>
          </View>
          
         
          <Text style={[general.subText3, { width: '100%', textAlign: 'center', marginTop: 20 }]}>Participe e faça o bem</Text>
          {
            this.props.navigation.state.params.EventoDetails.LinkExterno==null && this.state.diferencaDias==false 
            ?<Text style={[general.subText2, { width: '90%', textAlign: 'center', marginTop: 10, marginLeft: 'auto', marginRight: 'auto', fontSize: 14 }]}>Em breve você poderá adquirir os ingressos desse evento.</Text>
            :null
          }

          <View>
          {
            this.state.contribuir
            ?this.renderContribuir()
            :this.renderButtons()
          }
          </View>

        </ScrollView>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          >
          <LinearGradient colors={['#7a63b4', '#b552a7', '#f33697']} style={{ opacity: 0.5, width: '100%', height: '100%', position: 'absolute', }} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
          </LinearGradient>
          <TouchableOpacity onPress={ () => this.setState({ modalVisible: false })} style={{ position: 'absolute', top: 25, right: 15 }}>
            <Image source={require('../../assets/icons/closed.png')} style={{height: 20, resizeMode: 'contain'}}/>
          </TouchableOpacity>
          <View style={{ marginTop: 50, flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: metrics.basePadding*2, height: '100%', position: 'relative', zIndex: 9 }}>
          <LinearGradient style={{ width: 80, height: 80, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'relative', top: 20, zIndex: 999}} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
            <View style={{ width: 75, height: 75, borderRadius: 100, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.white }}>
            <Image source={require('../../assets/icons/checkbox.png')} style={{height: 30 , resizeMode: 'contain'}}/>
            </View>
          </LinearGradient>
            <View style={[styles.containerBox, { paddingTop: 40, paddingBottom: 50, height: 200, justifyContent: 'center', alignItems: 'center' }]}>
              <Text style={{ fontFamily: fonts.r400, color: colors.subTxt2, fontSize: 20, width: '100%', textAlign: 'center', marginTop: 15, }}>Contribuição Finalizada</Text>
              {/* <Text  style={{ fontFamily: fonts.r400, color: colors.subTxt1, fontSize: 16, width: '85%', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 20, marginLeft: 'auto', marginRight: 'auto', }}>Lembre-se de levar o celular com o voucher no dia do evento.</Text> */}
              <Text  style={{ fontFamily: fonts.r400, color: colors.subTxt1, fontSize: 16, width: '85%', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 20, marginLeft: 'auto', marginRight: 'auto', }}>{this.state.purchase}</Text>
            </View>
            <View style={[styles.BoxButton, { top: -30 }]}>
              <View style={[{ backgroundColor: 'rgba(0, 0, 0, 0.4)', width: '100%', height: 50, borderRadius: 30, }]}>
                <Buttons
                  title='VISUALIZAR VOUCHER'
                  width={'100%'}
                  gradient={true}
                  _onPress={ () => navigation.navigate("VoucherPage") }
                  />
              </View>
            </View>
        </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  eventos: state.eventos,
  contribuir: state.contribuir,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...EventosActions,
    ...ContribuirActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EventoPage);
