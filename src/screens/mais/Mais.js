import React, { Component } from 'react'
import { Linking, BackHandler, StatusBar, ScrollView, View, AsyncStorage } from 'react-native'

import Header from '../../components/Header'
import ListItemMais from '../../components/ListItemMais'

import styles from './Mais.styles'
import { general, colors } from '../../styles'
import SocialComponent from '../../components/Social';


class MaisPage extends Component {

  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    this.props.navigation.goBack()
  }

  removeLogin = () => {
    AsyncStorage.removeItem('token')
    this.props.navigation.navigate('LoginPage')
  }

  open = (open) => {
    Linking.canOpenURL(open).then(supported => {
      if (supported) {
        Linking.openURL(open);
      } else {
        console.log("Don't know how to open URI: " + this.props.url);
      }
    });
  };

  render() {
    const {navigation} = this.props
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
        <StatusBar backgroundColor='#EF9427' barStyle="light-content" />

        <Header
          title='Mais'
          background={['#EA5296', '#EF7571', '#F4A03F']}
          photoPerfil={true}
          nocadastro={true}
          navigation={this.props.navigation}
          />
            <ScrollView>
            <View style={styles.container}>
              <ListItemMais navigation={navigation}/>
              <View style={{justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', paddingHorizontal: 25, }}>
                <View style={styles.ulSocial}>
                  <SocialComponent
                    backgroundButton={true}
                    name='facebook-f'
                    _onPress={() => this.open('https://www.facebook.com/Tardezinha/')}
                    />
                  <SocialComponent
                    backgroundButton={true}
                    name='instagram'
                    _onPress={() => this.open('https://www.instagram.com/tardezinha/?hl=pt-br')}
                    />
                  <SocialComponent
                  backgroundButton={true}
                    name='twitter'
                    _onPress={() => this.open('https://twitter.com/tardezinhareal')}
                    />
                </View>
                <SocialComponent
                  backgroundButton={false}
                  _onPress={() => this.removeLogin()}
                  name='power-off'
                  />
              </View>

            </View>
          </ScrollView>
      </View>
    )
  }
}

export default MaisPage
