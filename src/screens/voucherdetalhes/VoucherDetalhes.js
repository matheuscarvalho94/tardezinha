import React, { Component } from 'react'
import { Text, View, Image, ScrollView } from 'react-native'
import QRCode from 'react-native-qrcode';

import Header from '../../components/Header'
import Voucher from '../../components/Voucher'
import styles from './VoucherDetalhes.styles'
import { colors, metrics, general } from '../../styles'

import moment from 'moment'

class VoucherDetalhesPage extends Component {

  state={
    dataresgate: null,
    datavalidade: null,
    text: 'http://facebook.github.io/react-native/',

  }

  componentDidMount(){
    const moment1 =  moment(this.props.navigation.state.params.voucherDetails.DataResgate).format("DD/MM/YYYY")
    const moment2 =  moment(this.props.navigation.state.params.voucherDetails.DataValidadeVoucher).format("DD/MM/YYYY")

    this.setState({
      dataresgate: moment1,
      datavalidade: moment2
    })
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
        <Header
          title='Voucher'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <ScrollView>
          <View style={[general.container,{padding: metrics.basePadding*2}]}>
            <View style={styles.boxQrcode}>
            {
              navigation.state.params.voucherDetails.Expirado==true
              ?<Image source={require('../../assets/icons/iconqrcode_quebrado.png')}
                style={{ height: 60, resizeMode: 'contain' }}
              />
              :<QRCode
                  value={this.state.text}
                  size={200}
                  bgColor='white'
                  fgColor={colors.pink}/>

            }


            </View>
            <View>
              <Text style={styles.h1}>{navigation.state.params.voucherDetails.Evento.Titulo}</Text>
              <Text style={styles.p}>{navigation.state.params.voucherDetails.Evento.Chamada}</Text>
              <Text style={styles.p}>{navigation.state.params.voucherDetails.DataValidadeVoucherExtenso}</Text>
            </View>

            <View style={styles.footer}>
              <View style={styles.containerfooter}>
                <Text style={styles.font1}>Data do Cadastro</Text>
                <Text style={styles.font2}>{this.state.dataresgate}</Text>
              </View>

              <View style={styles.containerfooter}>
                <Text style={styles.font1}>Válido até</Text>
                <Text style={styles.font2}>{this.state.datavalidade}</Text>
              </View>

            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default VoucherDetalhesPage
