import React, { Component } from 'react'
import { Text, View, FlatList, AsyncStorage } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';

import Header from '../../components/Header'
import ListArrecadacao from '../../components/ListArrecadacao'
import Loading from '../../components/Loading';

import styles from './Arrecadacao.styles'
import { colors, fonts, general } from '../../styles'
import { ENTRIES3 } from '../../json/arrecadacao';
import { ScrollView } from 'react-native-gesture-handler';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as ArrecadacaoActions } from '../../store/ducks/arrecadacao';

class ArrecadacaoPage extends Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token');
    this.props.getArrecadacaoRequest(token);
  }

  renderList = () => (
    <FlatList
      data={ENTRIES3}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item }) =>
        <ListArrecadacao
          data={item}
          navigation={this.props.navigation}
        />
      }
    />
  );

  render() {
    const { navigation, arrecadacao } = this.props;
    return (
      <View style={[general.structure]}>
      <Loading
        loading={this.props.arrecadacao.loading}
      />
      <Header
        title='Arrecadação'
        background={['#FB5424', '#FA8A00', '#EFCB00']}
        photoPerfil={true}
        nocadastro={true}
        goback={true}
        navigation={navigation}
      />
      {
        this.props.arrecadacao.loading
        ?null
        :<ScrollView>
          <View style={{ marginTop: 15 }}>
            <HTML 
              html={arrecadacao.data.Conteudo} 
              tagsStyles={{
                strong: { color: colors.pink },
                span: { color: colors.borderInput, },
                center: { width: '100%', textAlign: 'center' },
                div: { width: '95%', height: '100%', borderRadius: 10, backgroundColor: colors.white,  textAlign: 'center', marginTop: 30, paddingBottom: 50, borderWidth: 1, 
                borderColor: 'rgba(238,238,238, 0.7)', marginLeft: 'auto', marginRight: 'auto', },
                aside: { width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',  paddingHorizontal: 10, borderBottomColor: colors.borderInput, borderBottomWidth: 1,  listStyle: "none"  },
                
              }}
            />
          </View>
        </ScrollView>
      }
      </View>
    )
  }
}

// <ScrollView>
// <Text style={[general.subText1, {
//   width: '100%',
//   textAlign: 'center',
//   marginTop: 20,
//   marginBottom: 30,
// }]}> Valor arrecadado até o momento </Text>

// <View style={styles.wrapNumberTotal}>
//   <Text style={styles.h1}>R$</Text>
//   <Text style={[styles.h1, { color: colors.pink, marginLeft: 20 }]}>441.582,00</Text>
// </View>
// <View style={styles.wrapAcompanhe}>
//   <Text style={[general.subText1, {
//     fontSize: 13,
//     fontFamily: fonts.r700,
//   }]}>ACOMPANHE O EXTRATO</Text>
//   <Icon name='angle-down' size={20} color={colors.subTxt1} style={{marginLeft: 10}}/>
// </View>

// <View style={styles.wrapList}>
//   <View style={styles.box}>
//     {
//       this.renderList()
//     }
//   </View>
// </View>
// </ScrollView>

const mapStateToProps = state => ({
  arrecadacao: state.arrecadacao,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...ArrecadacaoActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ArrecadacaoPage);

