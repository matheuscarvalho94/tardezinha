import React, { Component } from 'react'
import { Text, Linking, ScrollView, View, AsyncStorage, StatusBar } from 'react-native'

import Header from '../../components/Header'
import Loading from '../../components/Loading';
import ListLojajinha from '../../components/ListLojinha';

import { ENTRIES2 } from '../../json/lojinha';
import styles from './Lojinha.styles'
import { general, colors } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as LojinhaActions } from '../../store/ducks/lojinha';

class LojinhaPage extends Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token')
    this.props.getLojinhaRequest(token)
  }

  handleOpenURL(url) {
    console.log('clicou')
    url?Linking.openURL(url):null
  }

  renderList = () => (
    <ListLojajinha
      data={this.props.lojinha.data}
      open={this.handleOpenURL}
    />
  );


  render() {
    return (
      <View style={[general.structure, { backgroundColor: colors.white }]}>
      <StatusBar backgroundColor='#EF9427' barStyle="light-content" />
      <Loading
          loading={this.props.lojinha.loading}
        />
        <Header
          title='Lojinha do Bem'
          background={['#EA5296', '#EF7571', '#F4A03F']}
          photoPerfil={true}
          navigation={this.props.navigation}
          nocadastro={true}
          />
          <ScrollView>
            <Text style={[general.subText1, {
              width: '100%',
              marginTop: 20,
              textAlign: 'center',
              marginBottom: 20,
            }]}> Encontre esses produtos na loja </Text>
            {
              this.renderList()
            }
          </ScrollView>
      </View>
    )
  }
}


const mapStateToProps = state => ({
  lojinha: state.lojinha,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...LojinhaActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LojinhaPage);
