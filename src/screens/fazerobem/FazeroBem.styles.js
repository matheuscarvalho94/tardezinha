import { StyleSheet } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  slide1: {
    flex: 1,
  },
  container: {
    padding: metrics.basePadding*2,
    backgroundColor: colors.backgroundBanner,
    height: 150,

  },
  img: {
      width: '100%',
      resizeMode: 'cover',
      height: 200
  },
  h1: {
      fontFamily: fonts.r700,
      fontSize: 18,
      color: colors.pink,
      marginBottom: 0,
  },
  p: {
      fontFamily: fonts.r400,
      fontSize: 16,
      color: colors.borderInput,
  },
  wrapHome: {
    marginTop: 30,
    padding: metrics.basePadding
  },
  buttonArrows: {
    borderRadius: 10,
    width: 43,
    height: 43,
    backgroundColor: colors.pink,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    top: -30,
  },
  img: {
    width: '100%',
    height: 400,
    resizeMode: 'contain',
  },
  slide1: {
    flex: 1,
    alignItems: 'center',
  },
  container2: {
    padding: metrics.basePadding*2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '95%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  h2: {
    fontFamily: fonts.r700,
    fontSize: 18,
    color: colors.facebook,
    marginBottom: 0,
  },
  p2: {
    fontFamily: fonts.r400,
    fontSize: 16,
    color: colors.borderInput,
    width: '100%',
    textAlign: 'center',
    marginBottom: 20
  },
});

export default styles;
