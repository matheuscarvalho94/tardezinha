import React, { Component } from 'react'
import { Text, ScrollView, View, AsyncStorage, StatusBar, FlatList, TouchableOpacity } from 'react-native'

import { general, colors } from '../../styles'
import styles from './FazeroBem.styles'

import Header from '../../components/Header';
import SwiperFazeroBem from '../../components/SwiperFazeroBem';
import Buttons from '../../components/Button'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as FazeroBemActions } from '../../store/ducks/fazerobem';

class FazeroBemPage extends Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token')
    this.props.getFazeroBemRequest(token)
  }

  renderSlide = () => (
    <SwiperFazeroBem
      data={this.props.fazerobem.data}
      navigation={this.props.navigation}
    />
  );

  render() {
    return (
      <View style={[general.structure ,{ backgroundColor: colors.white }]}>
        <StatusBar backgroundColor='#EF9427' barStyle="light-content" />
        <Header
        title='Fazer o Bem'
        background={['#EA5296', '#EF7571', '#F4A03F']}
        photoPerfil={true}
        navigation={this.props.navigation}
        nocadastro={true}/>
        <ScrollView>
          <View style={general.container}>
            <Text style={[general.subText1, {
              width: '100%',
              textAlign: 'center',
              marginBottom: 15,
            }]}> Veja as opções para ajudar mais </Text>
           {
             this.renderSlide()
           }
          </View>
        </ScrollView>

      </View>
    )
  }
}

const mapStateToProps = state => ({
  fazerobem: state.fazerobem,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...FazeroBemActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FazeroBemPage);
