import React, { Component } from 'react'
import { StatusBar, Text, View, TextInput, Alert, Image, TouchableOpacity, PermissionsAndroid, ActivityIndicator, Platform, NetInfo, BackHandler } from 'react-native'
import { withNavigationFocus } from "react-navigation";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import CheckBox from 'react-native-check-box'
import { TextInputMask } from 'react-native-masked-text';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import LinearGradient from 'react-native-linear-gradient';
import { GoogleSignin } from 'react-native-google-signin';
import * as RNIap from 'react-native-iap';

import Header from '../../components/Header';
import Buttons from '../../components/Button';
import Loading from '../../components/Loading';

import { general, colors } from '../../styles'
import styles from './Cadastro.styles'

import config from '../../config/config';
import { facebookLogin } from '../../services/authFacebook';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as CadastroActions } from '../../store/ducks/cadastro';
import { Creators as LoginActions } from '../../store/ducks/login';
import { Creators as UploadActions } from '../../store/ducks/upload';
import { Creators as FacebookActions } from '../../store/ducks/facebook';
import { Creators as AssinaturaActions } from '../../store/ducks/assinatura';
import { Creators as GoogleActions } from '../../store/ducks/google';

const options = {
  title: 'Escolha a sua foto de perfil',
  takePhotoButtonTitle: 'Tirar Foto',
  cancelButtonTitle: 'Cancelar',
  chooseFromLibraryButtonTitle: 'Escolher da Galeria',
  quality: 0.6,
  maxWidth: 1024,
  maxHeight: 1024,
  fixOrientation: true,
  storageOptions: {
      skipBackup: true,
      path: 'images',
      waitUntilSaved: true
  },
};

const itemSkus = Platform.select({
  ios: [
    'assinatura.tardizinha_001',
  ],
  android: [
    'assinatura.tardezinha_001',
  ],
});

class CadastroPage extends Component {

  constructor(props){
    super(props)

    this.state={
      Foto: '',
      Nome: '',
      Sobrenome: '',
      Email: '',
      Senha: '',
      ConfirmarSenha: '',
      CPF: '',
      Celular: '',
      Endereco: '',
      Cidade: '',
      Cep: '',
      dia: '',
      mes: '',
      ano: '',

      // Senha: '123mudar',
      // ConfirmarSenha: '123mudar',
      // CPF: '801.791.330-39',
      // Celular: '24998170414',
      // Endereco: 'Rua dois',
      // Cidade: 'brasilandia',
      // Cep: '27220-190',
      // dia: '01',
      // mes: '02',
      // ano: '1994',
      GoogleSubscriptionId: '',
      GooglePlayId: '',
      GooglePurchaseToken: '',
      AppStoreSubscriptionId: '',
      AppStoreId: '',
      AppStorePurchaseToken: '',
      Cadastro: true,
      IdFacebook: null,
      IdGoogle: null,
      AceitoConfirmacao: false,
      AceitoConfirmacao2: false,
      semconexao: false,
      loading: false,
      namephoto: '',
    }

    NetInfo.getConnectionInfo().then((connectionInfo) => {
      if(connectionInfo.type=='none'){
        this.setState({
          semconexao: true
        })
      }
    });
  }

  componentWillMount(){
    GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
      iosClientId: '97444700734-1rfmb9vcbuc93vkg412vkjnjcjo6637n.apps.googleusercontent.com',
      webClientId: '97444700734-bltqlrdp850cmb20nc09pdu58up634h3.apps.googleusercontent.com',
      offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
      accountName: '', // [Android] specifies an account name on the device that should be used
    })
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack()
    return true;
  }


  componentDidMount = async () => {
    try {
      const result = await RNIap.initConnection();
      await RNIap.consumeAllItems();
      console.log('result', result);
    } catch (err) {
      console.warn(err.code, err.message);
    }
    try {
      // const products = await RNIap.getProducts(itemSkus);
      const products = await RNIap.getSubscriptions(itemSkus);
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }



    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    console.log(this.props.navigation.state.params, 'LOG')
    if(this.props.navigation.state.params){
      if(this.props.navigation.state.params.dadosgoogle){
        this.setState({
          Foto: this.props.navigation.state.params.dadosgoogle.user.user.photo,
          Nome: this.props.navigation.state.params.dadosgoogle.user.user.givenName,
          Sobrenome: this.props.navigation.state.params.dadosgoogle.user.user.familyName,
          Email: this.props.navigation.state.params.dadosgoogle.user.user.email,
          IdGoogle: this.props.navigation.state.params.dadosgoogle.user.user.id,
        })
      }else if(this.props.navigation.state.params.dadosFb){
        this.setState({
          Foto: this.props.navigation.state.params.dadosFb.user.user.picture.data.url,
          Nome: this.props.navigation.state.params.dadosFb.user.user.first_name,
          Sobrenome: this.props.navigation.state.params.dadosFb.user.user.last_name,
          Email: this.props.navigation.state.params.dadosFb.user.user.email,
          IdFacebook: this.props.navigation.state.params.dadosFb.user.user.id,
        })
      }
    }
  }

  buyItem = async () => {
    console.log('clicando')
    if(Platform.OS==='android'){
      if(this.state.GooglePlayId==''){
      try {
        const purchase  = await RNIap.buySubscription(this.state.productList[0].productId);
        console.log(purchase, 'purchasseee')
        this.setState({
          GoogleSubscriptionId: purchase.productId,
          GooglePlayId: purchase.transactionId,
          GooglePurchaseToken: purchase.purchaseToken,
        })
        // this.handleCadastro()
        this.cadastroSucess()

      } catch (err) {
        console.log(err.code, err.message, 'ERRO');
      }
      }else{
        // this.handleCadastro()
        Alert.alert(
          config.nameApp,
          "Assinatura não concluida",
          [
            {text: 'OK', onPress: () => navigation.goBack()},
        ])
      }
    }else if(Platform.OS==='ios'){
      if(this.state.AppStorePurchaseToken==''){
        try {
          const purchase  = await RNIap.buyProduct(this.state.productList[0].productId);
          console.log(purchase, 'purchasseee')
          this.setState({
            AppStoreSubscriptionId: purchase.productId,
            AppStoreId: purchase.transactionId,
            AppStorePurchaseToken: purchase.transactionReceipt,
          })

          this.cadastroSucess()
          // this.handleCadastro()
        } catch (err) {
          console.log(err.code, err.message, 'ERRO');
        }
      }else{
        Alert.alert(
          config.nameApp,
          "Assinatura não concluida",
          [
            {text: 'OK', onPress: () => navigation.goBack()},
          ])
        // this.handleCadastro()
      }
    }
  }



  requestGaleriaPermission = async () => {
    try {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
            'title': 'Cool Photo App READ_EXTERNAL_STORAGE Permission',
            'message': 'Cool Photo App needs access to your READ_EXTERNAL_STORAGE ' +
                'so you can take awesome pictures.'
        }
      )
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
            'title': 'Cool Photo App CAMERA Permission',
            'message': 'Cool Photo App needs access to your CAMERA ' +
                'so you can take awesome pictures.'
        }
      )
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
            'title': 'Cool Photo App WRITE_EXTERNAL_STORAGE Permission',
            'message': 'Cool Photo App needs access to your WRITE_EXTERNAL_STORAGE ' +
                'so you can take awesome pictures.'
        }
      )
    } catch (err) {
        console.warn(err)
    }
  }


  handleCamera = () => {
    if(Platform.OS=='android'){
      this.requestGaleriaPermission()
    }
    ImagePicker.showImagePicker(options, (response) => {
      console.log(response, 'ffoto')
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      } else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      } else {
        if (Platform.OS == 'ios'){
          this.setState({
            Foto: response.uri,
            namephoto: response.fileName,
            loading: true,
          }
        );

        }else {
          this.setState({
            Foto: response.path,
            namephoto: response.fileName,
            loading: true,
          }
         );
        }
        this.props.getUploadRequest(this.state)
      }
    })
  }

  handleFacebook = async () => {
    // const { getLoginFBRequest } = this.props;
    const response = await facebookLogin();
    if (response.error) {
      this.setState({ error: response.error });
      //console.log(response.error);
      return false;
    }
    console.log(response, 'INFOS FACE');
    // this.props.getFacebookRequest(response);
    this.setState({
      Foto: response.user.picture.data.url,
      Nome: response.user.first_name,
      Sobrenome: response.user.last_name,
      Email: response.user.email,
      IdFacebook: response.user.id,
    })
  }

  handleGoogle = async () => {
    this.setState({
      loading: true
    })
    GoogleSignin.signIn()
    .then((user) => {
      console.log(user);
      this.setState({
        Nome: user.user.givenName,
        Sobrenome: user.user.familyName,
        Email: user.user.email,
        Foto: user.user.photo,
        IdGoogle: user.user.id,
      })
      this.setState({user: user});
      this.setState({
        loading: false
      })
    })
    .catch((err) => {
      this.setState({
        loading: false
      })
      console.log('WRONG SIGNIN', err);
    })
    .done();
  };

  searchCep = (c) => {
    let dataJson = []
    let r = c.replace("-", "");
    let cepFormatado = r;
    if(c.length == 9){
    return axios.get('https://viacep.com.br/ws/' + cepFormatado + '/json')
    .then((response) => {
      console.log(response, 'RESPONSE')
      if(response.data.erro!=true){
        dataJson=response
        this.setState({
          Cep: dataJson.data.cep,
          Cidade: dataJson.data.localidade,
          Endereco: dataJson.data.bairro + ' - ' + dataJson.data.logradouro
        })
      }else if(response.data.erro==true){
        this.setState({
          Cep: '',
          Cidade: '',
          Endereco: ''
        })
      }
    })
    }else{
    console.log('não deu seis numero')
    }
  }

  validation = () => {
    if (!this.state.Nome) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Nome.'
      )
    }else if (!this.state.Sobrenome) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Sobrenome.'
      )
    }else if (!this.state.CPF) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu CPF.'
      )
    }else if (!this.state.Celular) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Celular.'
      )
    }else if (!this.state.Email) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu E-mail.'
      )
    }else if (!this.state.Senha) {
      return Alert.alert(
        config.nameApp,
        'Preencha sua Senha.'
      )
    }else if (!this.state.ConfirmarSenha) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Confirmar Senha.'
      )
    }else if (!this.state.Cep) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Cep.'
      )
    }else if (!this.state.Endereco) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Endereco.'
      )
    }else if (!this.state.Cidade) {
      return Alert.alert(
        config.nameApp,
        'Preencha sua Cidade.'
      )
    }else if (!this.state.dia) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Dia.'
      )
    }else if (!this.state.mes) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Mês.'
      )
    }else if (!this.state.ano) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Ano.'
      )
    } else if(!this.state.AceitoConfirmacao){
      return Alert.alert(
        config.nameApp,
        'É necessário aceitar os termos do programa.'
      )
    } else if(!this.state.AceitoConfirmacao2){
      return Alert.alert(
        config.nameApp,
        'É necessário aceitar os termos de compra.'
      )
    }
    return true
  }

  handleCadastro = () => {
    let mensagem = this.validation()
    if (mensagem == true) {
      if(this.state.semconexao==true){
        this.props.navigation.navigate('SemInternet')
      }else{
        this.props.getCadastroRequest(this.state);
      }
    }
  }

  cadastroSucess = async () => {
    const { navigation } = this.props;
    navigation.navigate('CadastroSucessoPage', {
      state: this.state
    })
  }

  async componentWillReceiveProps(props){
    const { cadastro, upload, login } = props;
    if(cadastro.error==true){
      cadastro.loading=false
      Platform.OS==='ios'
      ? setTimeout(() => {
        Alert.alert(config.nameApp, cadastro.error_message)
        cadastro.error=false;
      }, 100)
      :await Alert.alert(config.nameApp, cadastro.error_message)
      cadastro.error=false;
    }else if(cadastro.data === 'Cadastro realizado com sucesso'){
      this.cadastroSucess()
      // this.buyItem()
    }else if(cadastro.data === 'Cadastro realizado com sucesso. Sua conta está inativa, realize a assinatura para acessar o app.'){
      this.cadastroSucess()
      // Alert.alert(
      //   config.nameApp,
      //   "Assinatura não concluida",
      //   [
      //     {text: 'OK', onPress: () => this.props.navigation.goBack()},
      // ])
      // this.buyItem()
    }else if(upload.data.Arquivo){
      console.log(upload.data, 'loggg')
      this.setState({ Foto: upload.data.Arquivo, loading: false })
    }
  }

  render() {
    return (
      <View style={[styles.structure, { backgroundColor: '#fff'}]}>
        <StatusBar backgroundColor={colors.pink} barStyle="light-content" />
        <Header
          title='Cadastrar'
          background={['#7a63b4', '#b552a7', '#f33697']}
          photoPerfil={false}
          redux={false}
          nocadastro={false} />
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
        >
          <Loading
            loading={this.props.cadastro.loading}
          />
          <Loading
            loading={this.state.loading}
          />
          <View style={styles.container}>

            <View style={styles.wrapInformacoes}>

              <Text style={general.subText1}>Agilize o cadastro com</Text>

              <TouchableOpacity style={[ styles.buttonSocial, { backgroundColor: colors.facebook, marginTop: 20 } ]} onPress={this.handleFacebook}>
                <Icon
                  name='facebook-f'
                  size={22}
                  color={colors.white}
                  style={styles.iconSocial}
                />
                <Text style={ styles.txtSocial }>LOGIN COM FACEBOOK</Text>
              </TouchableOpacity>

              {/* <TouchableOpacity style={[ styles.buttonSocial, { backgroundColor: 'green', marginTop: 20 } ]} onPress={() => this.buyItem()}>
                <Text style={ styles.txtSocial }>COMPRAR LICENÇA</Text>
              </TouchableOpacity> */}

              <TouchableOpacity style={[ styles.buttonSocial, { backgroundColor: '#E73458' } ]} onPress={this.handleGoogle}>
                <Image
                  source={require('../../assets/icons/google.png')}
                  style={styles.iconSocial2}
                />
                <Text style={ styles.txtSocial }>LOGIN COM GOOGLE</Text>
              </TouchableOpacity>

              <View style={styles.wrapOu}>
                <View style={styles.border}></View>
                <View style={styles.wrapOuTxt}>
                  <Text style={styles.txtOu}>OU</Text>
                </View>
              </View>

              </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 30, fontSize: 19}]}> Informação Pessoal </Text>
              <View style={{
                width: 125,
                height: 125,
                position: 'relative',
                top: 0,
                marginLeft: 'auto',
                marginRight: 'auto',
                marginBottom: 45,
              }}>
                <LinearGradient style={{
                  width: 125,
                  height: 125,
                  borderRadius: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                }} colors={['#f33697', '#b552a7', '#7a63b4']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
                  <TouchableOpacity style={styles.rowFoto} onPress={() => this.handleCamera()}>
                    <ActivityIndicator style={{ position: 'absolute', left: 50, top: 45 }}/>
                  {
                    this.state.Foto
                    ?<Image
                      source={{uri: this.state.Foto}}
                      style={[styles.photo, { position: 'relative', zIndex: 1 }]}
                    />
                    :<View style={{ width: '100%', height: '100%', backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center'}}><Icon
                      name='user'
                      size={75}
                      color={colors.purple}
                      style={{ position: 'relative', zIndex: 1 }}
                    /></View>
                  }

                  </TouchableOpacity>

                </LinearGradient>
                <TouchableOpacity style={{ width: 30, height: 30, backgroundColor: colors.pink, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 10, right: 0, zIndex: 99 }}  onPress={() => this.handleCamera()}>
                <Icon
                name='plus'
                size={15}
                color={colors.white}
                />
              </TouchableOpacity>
              </View>
              <View style={general.row}>
                <Text style={general.label}> NOME* </Text>
                <TextInput
                  style={general.textInput}
                  placeholder='Digite seu nome'
                  placeholderTextColor={colors.borderInput}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  value={this.state.Nome}
                  onChangeText={Nome => this.setState({ Nome })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.sobrenome.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> SOBRENOME* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.sobrenome = input; }}
                  placeholder='Digite seu sobrenome'
                  placeholderTextColor={colors.borderInput}
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Sobrenome}
                  onChangeText={Sobrenome => this.setState({ Sobrenome })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.ipt_cpf.getElement().focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> CPF* </Text>
                <TextInputMask
                  style={general.textInput}
                  ref={(input) => { this.ipt_cpf = input; }}
                  placeholder='Digite seu cpf'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.CPF}
                  onChangeText={CPF => this.setState({ CPF })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.celular.getElement().focus();}}
                  type={'cpf'}
                  maxLength={14}
                  options={{
                      format: '999.999.999-99'
                  }}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> CELULAR* </Text>
                <TextInputMask
                  style={general.textInput}
                  ref={(input) => { this.celular = input; }}
                  placeholder='Digite seu celular'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Celular}
                  onChangeText={Celular => this.setState({ Celular })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.email.focus();}}
                  type={'cel-phone'}
                  options={{
                      format: '(99) 99999-9999'
                  }}
                  />
              </View>

            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Segurança </Text>

              <View style={general.row}>
                <Text style={general.label}> EMAIL* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.email = input; }}
                  placeholder='Digite seu email'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Email}
                  onChangeText={Email => this.setState({ Email })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.senha.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> SENHA* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.senha = input; }}
                  placeholder='Digite seu senha'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Senha}
                  secureTextEntry={true}
                  onChangeText={Senha => this.setState({ Senha })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.confirmasenha.focus();}}
                  />
              </View>
              <View style={general.row}>
                <Text style={general.label}> CONFIRMAR SENHA* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.confirmasenha = input; }}
                  placeholder='Digite seu senha'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.ConfirmarSenha}
                  secureTextEntry={true}
                  onChangeText={ConfirmarSenha => this.setState({ ConfirmarSenha })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.Cepp.getElement().focus();}}
                  />
              </View>

            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Endereço </Text>

              <View style={general.row}>
                <Text style={general.label}> CEP * </Text>
                <TextInputMask
                  style={general.textInput}
                  ref={(input) => { this.Cepp = input; }}
                  placeholder='Digite seu cep'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Cep}
                  onSubmitEditing={()=>{this.endereco.focus();}}
                  onChangeText={this.searchCep.bind(this)}
                  type={'zip-code'}
                  options={{
                      format: '9999-999'
                  }}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> ENDEREÇO * </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.endereco = input; }}
                  placeholder='Digite seu endereço'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Endereco}
                  onChangeText={Endereco => this.setState({ Endereco })}
                  onSubmitEditing={()=>{this.cidade.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> CIDADE * </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.cidade = input; }}
                  placeholder='Digite seu cidade'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Cidade}
                  onSubmitEditing={()=>{this.dia.focus();}}
                  onChangeText={Cidade => this.setState({ Cidade })}
                  />
              </View>



            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Aniversário </Text>
              <View style={styles.wrapAniversario}>

                <View style={[general.row, { width: '30%', borderBottomColor: colors.borderInput, borderBottomWidth: 1, paddingBottom: 0 }]}>
                  <Text style={general.label}> DIA * </Text>
                  <TextInput
                    style={[general.textInput, { borderBottomWidth: 0 }]}
                    ref={(input) => { this.dia = input; }}
                    placeholder='Dia'
                    placeholderTextColor={colors.borderInput}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType={'next'}
                    keyboardType={"numeric"}
                    underlineColorAndroid="transparent"
                    value={this.state.dia}
                    maxLength={2}
                    onSubmitEditing={()=>{this.mes.focus();}}
                    onChangeText={dia => this.setState({ dia })}
                    />
                </View>

                <View style={[general.row, { width: '30%', borderBottomColor: colors.borderInput, borderBottomWidth: 1, paddingBottom: 0 }]}>
                  <Text style={general.label}> MÊS * </Text>
                  <TextInput
                    style={[general.textInput, { borderBottomWidth: 0 }]}
                    ref={(input) => { this.mes = input; }}
                    placeholder='Mês'
                    maxLength={2}
                    placeholderTextColor={colors.borderInput}
                    autoCapitalize="none"
                    keyboardType={"numeric"}
                    autoCorrect={false}
                    returnKeyType={'next'}
                    underlineColorAndroid="transparent"
                    value={this.state.mes}
                    onSubmitEditing={()=>{this.ano.focus();}}
                    onChangeText={mes => this.setState({ mes })}
                    />
                </View>

                <View style={[general.row, { width: '30%', borderBottomColor: colors.borderInput, borderBottomWidth: 1, paddingBottom: 0 }]}>
                  <Text style={general.label}> ANO * </Text>
                  <TextInput
                    style={[general.textInput, { borderBottomWidth: 0 }]}
                    ref={(input) => { this.ano = input; }}
                    placeholder='Ano'
                    maxLength={4}
                    placeholderTextColor={colors.borderInput}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType={"numeric"}
                    returnKeyType={'next'}
                    underlineColorAndroid="transparent"
                    value={this.state.ano}
                    onSubmitEditing={()=>{this.ano.focus();}}
                    onChangeText={ano => this.setState({ ano })}
                    />
                </View>

              </View>

            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Termos de uso </Text>

              <View style={[general.row, {flexDirection: 'row', paddingRight: 10,}]}>
                <CheckBox
                  style={{marginTop: 2, marginRight: 2, borderRadius: 6,}}
                  uncheckedCheckBoxColor={colors.pink}
                  checkedCheckBoxColor={colors.pink}
                  isChecked={this.state.AceitoConfirmacao}
                  onClick={() => this.setState({
                      AceitoConfirmacao: !this.state.AceitoConfirmacao
                  })}
                />
                <View style={{ flexDirection: 'column' , marginBottom: 20,}}>
                  <Text style={styles.txtAceitoTermos}>Aceito os <Text onPress={() => { this.props.navigation.navigate('TermoUso') }} style={styles.sublinhado}>
                  Termos de Uso</Text> do programa.</Text>
                </View>
              </View>

              <View style={[general.row, {flexDirection: 'row', paddingRight: 10,}]}>
                <CheckBox
                  style={{marginTop: 2, marginRight: 2, borderRadius: 6,}}
                  uncheckedCheckBoxColor={colors.pink}
                  checkedCheckBoxColor={colors.pink}
                  isChecked={this.state.AceitoConfirmacao2}
                  onClick={() => this.setState({
                      AceitoConfirmacao2: !this.state.AceitoConfirmacao2
                  })}
                />
                <View style={{ flexDirection: 'column' }}>
                  <Text style={styles.txtAceitoTermos}>Aceito os <Text onPress={() => { this.props.navigation.navigate('TermoUso') }} style={styles.sublinhado}>
                  Termos de compra</Text> do programa.</Text>
                </View>
              </View>
            </View>

            <View style={styles.BoxButton}>
              <View style={styles.border}></View>
              <View style={{ marginTop: -20, marginLeft: Platform.OS==='android'?110:80 }}>
                <Buttons
                  title='CADASTRAR'
                  icon='arrow-right'
                  width={200}
                  gradient={true}
                  _onPress={() => this.handleCadastro()}
                  />
                </View>
            </View>

          </View>

        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  cadastro: state.cadastro,
  login: state.login,
  upload: state.upload,
  facebook: state.facebook,
  google: state.google,
  assinatura: state.assinatura,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...CadastroActions,
    ...LoginActions,
    ...FacebookActions,
    ...GoogleActions,
    ...UploadActions,
    ...AssinaturaActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)( withNavigationFocus(CadastroPage));
