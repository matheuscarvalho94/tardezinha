import React, { Component } from 'react'
import { Alert, Text, View, ImageBackground, Image, TextInput, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient'

import { general, metrics, colors, fonts } from '../../styles'
import styles from '../login/Login.styles'
import Buttons from '../../components/Button'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as LoginActions } from '../../store/ducks/login';

class CadastroSucessoPage extends Component {

  componentDidMount = async () => {
    console.log(this.props.navigation.state.params.state, 'ESTADO CADASTRO')
    this.props.getLoginRequest(this.props.navigation.state.params.state)
  }

  async componentWillReceiveProps(props){
    const { login } = props;
    if(login.data.access_token){
      console.log('LOGINNNN')
    }
  }

  render() {
    const { navigation } = this.props;
    return (

      <ImageBackground source={require('../../assets/background/1.jpg')} style={{ width: '100%', height: '100%', position: 'relative' }}>
        <ScrollView>
        <TouchableOpacity onPress={ () => {navigation.navigate('HomePage')}} style={{ position: 'absolute', top: 25, right: 15 }}>
        <Image source={require('../../assets/icons/closed.png')} style={{height: 20, resizeMode: 'contain'}}/>
        </TouchableOpacity>
        <View style={{ marginTop: 100, flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: metrics.basePadding*2, height: '100%', }}>
            <LinearGradient style={{ width: 80, height: 80, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'relative', top: 20, zIndex: 999}} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
              <View style={{ width: 75, height: 75, borderRadius: 100, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.white }}>
                <Image source={require('../../assets/icons/checkbox.png')} style={{height: 30 , resizeMode: 'contain'}}/>
                </View>
            </LinearGradient>
              <View style={[styles.containerBox, { paddingTop: 40, height: 180,}]}>
                <Text style={{ fontFamily: fonts.r400, color: colors.subTxt2, fontSize: 25, width: '100%', textAlign: 'center', marginTop: 0, }}>Cadastro Completo</Text>
                <Text  style={{ fontFamily: fonts.r400, color: colors.subTxt1, fontSize: 16, width: '90%', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 15, marginLeft: 'auto', marginRight: 'auto', }}>Você já pode acessar o Tardezinha</Text>
              </View>
              <View style={[styles.BoxButton, { top: -30 }]}>
                <Buttons
                  title='IR PARA INÍCIO'
                  width={'100%'}
                  gradient={true}
                  _onPress={ () => navigation.navigate('HomePage') }
                  />
              </View>
          </View>
        </ScrollView>
      </ImageBackground>
    )
  }
}

const mapStateToProps = state => ({
  login: state.login,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...LoginActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CadastroSucessoPage);

