import { StyleSheet } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
    wrapper: {
      height: '100%',
    },
    Background: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
    },
    slide1: {
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    img: {
        height: 200,
        resizeMode: 'contain',
        marginTop: 90,
        marginBottom: 80,
    },
    h1: {
        fontFamily: fonts.r700,
        fontSize: 20,
        color: colors.white,
        marginBottom: 30,
        width: 260,
        textAlign: 'center',
    },
    p: {
        fontFamily: fonts.r400,
        fontSize: 18,
        color: colors.white,
        width: '80%',
        textAlign: 'center',
    },
    footer:{
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        zIndex: 10,
        height: 60,
        width: '100%',
        paddingHorizontal: metrics.basePadding*2,
    },
    footer_txt: {
        fontFamily: fonts.r700,
        color: colors.white,
        fontSize: 18
    },

});

export default styles;
