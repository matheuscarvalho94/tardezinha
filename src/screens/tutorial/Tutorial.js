import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, BackHandler, TouchableOpacity, AsyncStorage, StatusBar, Dimensions } from 'react-native'
import Swiper from 'react-native-swiper';

import { general, colors } from '../../styles';
import styles from './Tutorial.styles';

class TutorialPage extends Component {

  state = {
    sliderLength: null,
    button_Up: 'PULAR',
    button_Next: 'PRÓXIMO'
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.refs.swiper.scrollBy(-1) // works best when the goBack is async
    return true;
  }

  _onNext = async () => {
    await AsyncStorage.setItem('getFirst', 'true')
    this.props.navigation.navigate('LoginPage')
  }

  _onSwipe = (index) => {
    this.setState({
      sliderLength: index
    })
    console.log('index changed', index);
    if(index==3){
      this.setState({
        button_Next: 'INICIAR',
        button_Up: ''
      })
    }else{
      this.setState({
        button_Up: 'PULAR',
        button_Next: 'PRÓXIMO'
      })
    }
  }

  _swipe = (n) => {
    console.log(this.state.sliderLength, 'number slide')
    if(this.state.sliderLength==3){
      this._onNext()
    }else{
      this.refs.swiper.scrollBy(n)
    }
  }

  render() {
    return (
      <View style={[general.structure, { position: 'relative', width: '100%', height: '100%' }]}>
        <StatusBar backgroundColor={colors.pink} barStyle="light-content" />
        <View style={styles.footer}>
          <TouchableOpacity onPress={() => this._onNext()}>
            <Text style={styles.footer_txt}>{this.state.button_Up}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this._swipe(1)}>
            <Text style={styles.footer_txt}>{this.state.button_Next}</Text>
          </TouchableOpacity>
        </View>
        <Swiper
          onIndexChanged={this._onSwipe}
          ref='swiper'
          dot={
            <View style={{backgroundColor:'#fff', width: 8, height: 8,borderRadius: 100, marginLeft: 6, marginRight: 6, marginTop: 6, marginBottom: 0,}} />
          }
          activeDot={
            <View style={{backgroundColor: '#FACC0F', width: 12, height: 12, borderRadius: 100, marginLeft: 3, marginRight: 3, marginTop: 6, marginBottom: 0,}} />
          }
          showsButtons={false}
          loop={false}
          style={styles.wrapper}
          showsPagination={true}
          showsButtons={false}>
          <View style={styles.slide1}>
            <ImageBackground
              source={require('../../assets/background/tutorial/1.jpg')}
              style={styles.Background}
            >
              <View style={
                styles.container
              }>
              <Image source={require('../../assets/icons/1.png')} style={styles.img}/>
              <Text style={styles.h1}>OBTENHA ACESSO A CONTEÚDOS EXCLUSIVOS</Text>
              <Text style={styles.p}>Aqui você fica por dentro de tudo o que está acontecendo nos bastidores.</Text>
              </View>
            </ImageBackground>
          </View>

          <View style={styles.slide1}>
            <ImageBackground
              source={require('../../assets/background/tutorial/2.jpg')}
              style={styles.Background}
            >
              <View style={
                styles.container
              }>
              <Image source={require('../../assets/icons/2.png')} style={styles.img}/>
              <Text style={styles.h1}>ACOMPANHE A AGENDA DE SHOWS</Text>
              <Text style={styles.p}>Confira tudo que acontece na turnê de despedida do melhor fim de tarde do Brasil!</Text>
              </View>
            </ImageBackground>
          </View>

          <View style={styles.slide1}>
            <ImageBackground
              source={require('../../assets/background/tutorial/3.jpg')}
              style={styles.Background}
            >
              <View style={
                styles.container
              }>
              <Image source={require('../../assets/icons/3.png')} style={styles.img}/>
              <Text style={styles.h1}>COMPRE O INGRESSO E O KILO DE ALIMENTO</Text>
              <Text style={styles.p}>Tenha acesso as arrecadações e faça o bem de diversas maneiras.</Text>
              </View>
            </ImageBackground>
          </View>

          <View style={styles.slide1}>
            <ImageBackground
              source={require('../../assets/background/tutorial/4.jpg')}
              style={styles.Background}
            >
              <View style={
                styles.container
              }>
              <Image source={require('../../assets/icons/4.png')} style={styles.img}/>
              <Text style={styles.h1}>ACOMPANHE O TARDEZINHA SOCIAL</Text>
              <Text style={styles.p}>Veja como estão as ações e contribuições do Tardezinha Social.</Text>
              </View>
            </ImageBackground>
          </View>
        </Swiper>


      </View>
    )
  }
}

export default TutorialPage;
