import React, { Component } from 'react'
import { Text, View, Image, ScrollView, AsyncStorage, BackHandler } from 'react-native'
import HTML from 'react-native-render-html';

import Header from '../../components/Header'
import Loading from '../../components/Loading';

import styles from './Sobre.styles'
import { colors, metrics, fonts, general } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as SobreActions } from '../../store/ducks/sobre';

class SobrePage extends Component {
  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token');
    this.props.getSobreRequest(token)
  }

  handleBackButton = () => {
    this.props.navigation.goBack()
  }

  render() {
    const {navigation, sobre} = this.props
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
      <Loading
        loading={this.props.sobre.loading}
      />
        <Header
          title={sobre.data.Titulo}
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />

        {
          this.props.sobre.loading
          ?null
          :<ScrollView>
            <Text style={[general.subText3, { paddingHorizontal: metrics.basePadding*2}]}>{sobre.data.Subtitulo}</Text>
            <View style={styles.box}>
              <HTML
                html={sobre.data.Conteudo}
                tagsStyles={{
                  strong: { color: colors.pink, fontSize: 16, marginBottom: 10 },
                  p: { color: colors.borderInput, fontSize: 14, fontFamily: fonts.r400, marginBottom: 10 }
                }} 
              />
            </View>
            <Image
              source={{uri: sobre.data.Imagem}}
              style={styles.fullimage}
              />
          </ScrollView>
        }

      </View>
    )
  }
}

const mapStateToProps = state => ({
  sobre: state.sobre,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...SobreActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SobrePage);

