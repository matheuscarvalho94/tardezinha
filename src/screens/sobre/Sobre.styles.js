import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  fullimage: {
    resizeMode: 'contain',
    height: 240,
    width: '100%',
    marginTop: -50,
    position: 'relative',
    zIndex: 0
  },
  box: {
    width: '90%',
    backgroundColor: colors.white,
    borderRadius: 10,
    borderColor: 'rgba(238,238,238, 0.7)',
    borderWidth: 1,
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: metrics.basePadding*2,
    position: 'relative',
    zIndex: 1,
  },
  h2: {
      fontFamily: fonts.r700,
      color: colors.pink,
      fontSize: 18,
      marginBottom: 35,
  },
});

export default styles;
