import React, { Component } from 'react'
import { FlatList, View } from 'react-native'

import Header from '../../components/Header'
import ListSongs from '../../components/ListSongs'
import Player from '../../components/Player'
import {ENTRIES5} from '../../json/musicas'

import styles from './Musicas.styles'
import { colors, metrics, general } from '../../styles'


class MusicasPage extends Component {
  _playPause() {
    if(this.props.state == TrackPlayer.STATE_PAUSED) {
        TrackPlayer.play();
    } else {
        TrackPlayer.pause();
    }
  }

  async _previous() {
    // TODO add tracks to the queue
  }

  _next() {
      // TODO add tracks to the queue
  }


  renderList = () => (
    <FlatList
      data={ENTRIES5}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item }) =>
        <ListSongs
          data={item}
        />
      }
    />
  );

  render() {
    const { navigation } = this.props;
    return (
      <View style={[ general.structure, {backgroundColor: colors.white } ]}>
        <Header
          title='Músicas Exclusivas'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <View style={[general.container, { marginTop: 20}]}>
          {
            this.renderList()
          }
        </View>
        <Player />
      </View>
    )
  }
}

export default MusicasPage
