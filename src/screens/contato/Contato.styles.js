import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  buttonAssunto: {
    height: 39,
    paddingBottom: 10,
    marginTop: 0,
    marginBottom: 20,
    width: '100%',
    backgroundColor: 'transparent',
    borderBottomColor: colors.borderInput,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textAssunto: {
    fontSize: 16,
    color: colors.borderInput,
    fontFamily: fonts.r400,
  },

  closeButtonText: {
    color: '#027afe'
   },
   containerPicker: {
    position: 'absolute',
    bottom: 0,
    marginTop: 60,
    backgroundColor: '#fff',
    width: '100%',
    height: 200
  },
  closeButtonContainer: {
   flexDirection: 'row',
    justifyContent: 'flex-end',
    borderTopColor: '#e2e2e2',
    borderTopWidth: 1,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth:1
  },
  closeButton: {
    paddingRight:10,
     paddingTop:10,
     paddingBottom:10
  },
  closeButtonText: {
    color: '#027afe'
  },
});

export default styles;
