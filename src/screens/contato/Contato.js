import React, { Component } from 'react'
import { Text, View, TextInput, Picker, PickerIOS, Modal, TouchableOpacity, AsyncStorage, TouchableHighlight, Alert, Platform } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/FontAwesome';

import Header from '../../components/Header'
import Buttons from '../../components/Button'
import Loading from '../../components/Loading';

import styles from './Contato.styles';
import { general, metrics, colors } from '../../styles'
import config from '../../config/config';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as FaleConoscoActions } from '../../store/ducks/faleconosco';
import { Creators as UserActions } from '../../store/ducks/user';

class ContatoPage extends Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token')
    this.props.getUserRequest(token)
    this.setState({
      token: token
    })
  }

  async componentWillReceiveProps(props){
    const {user, faleconosco} = props;
    this.setState({
      Nome: user.data.Nome,
      Email: user.data.Email,
      Estado: 'Rio de janeiro',
      Cidade: 'barra mansa',
      CPF: user.data.CPF,
      Empresa: 'am4',
      Assunto: '',
      Mensagem: '',
    })

    if(faleconosco.data=='Agradecemos seu contato. Ele será analisado e retornado por e-mail.'){
      faleconosco.loading=false
      setTimeout(() => {
        Alert.alert(config.nameApp, 'Agradecemos seu contato. Ele será analisado e retornado por e-mail.',
        [
          {text: 'OK', onPress: () => {this.goBack()}},
        ])
      }, 100);
    }

    // if(fale)
  }

  goBack = () => {
    this.props.navigation.goBack()
    this.props.faleconosco.data='';
  }

  state={
    token: '',
    openPicker1: false,
    Nome: '',
    Email: '',
    Estado: '',
    Cidade: '',
    CPF: '',
    Empresa: '',
    Assunto: '',
    Mensagem: '',
  }

  changeAssunto(time){
    console.log(time, 'select ')
    this.setState({
      openPicker1: false,
      Assunto: time,
    })
  }

  validation = () => {
    if (this.state.Email=='') {
      return Alert.alert(
        config.nameApp,
        'Preencha o assunto.'
      )
    }else if (this.state.Mensagem=='') {
      return Alert.alert(
        config.nameApp,
        'Preencha a mensagem.'
      )
    }
    return true
  }

  handleContato = () => {
    let mensagem = this.validation()
    if (mensagem == true) {
      this.props.getFaleConoscoRequest(this.state)
    }
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={[general.structure, {backgroundColor: colors.white}]}>
      <Loading
        loading={this.props.faleconosco.loading}
      />
        <Header
          title='Contato'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
        />
        <KeyboardAwareScrollView>
        <View style={[general.container, {padding: metrics.basePadding*2}]}>
          <View style={general.row}>
            <Text style={general.label}> E-MAIL* </Text>
            <TextInput
              style={[general.textInput, {borderBottomWidth: 0}]}
              placeholder='dudinhaamaral@gmail.com'
              placeholderTextColor={colors.borderInput}
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType={'next'}
              underlineColorAndroid="transparent"
              value={this.state.Email}
              onChangeText={Email => this.setState({ Email })}
              />
          </View>
          <View style={[general.row, {borderBottomWidth: Platform.OS==='android'?1:0,
            borderBottomColor: colors.borderInput,}]}>

            {
              Platform.OS==='android'
              ?<Picker
                style={[general.pickerInput, {marginBottom: 0}]}
                placeholder='Assunto'
                placeholderTextColor={colors.borderInput}
                selectedValue={this.state.Assunto}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({Assunto: itemValue})
                }>
                <Picker.Item label="Informações" value="Informações" />
                <Picker.Item label="Sugestões" value="Sugestões" />
                <Picker.Item label="Dúvidas" value="Dúvidas" />
                <Picker.Item label="Agradecimentos" value="Agradecimentos" />
                <Picker.Item label="Reclamações" value="Reclamações" />
                <Picker.Item label="Outros" value="Outros" />
              </Picker>
              :<TouchableOpacity style={[styles.buttonAssunto]} onPress={() => { this.setState({ openPicker1: true }) }}>
                {
                  this.state.Assunto==''
                  ?<Text style={styles.textAssunto}>
                    Assunto
                  </Text>
                  :<Text style={styles.textAssunto}>
                  {this.state.Assunto}
                  </Text>
                }
              </TouchableOpacity>
            }
            <Icon name='angle-down' size={20} color={colors.borderInput} style={{position: 'absolute', right: 0, top: Platform.OS==='android'?15:5}} />

        </View>

          <View style={[general.row, {marginTop: 10}]}>
            <TextInput
              style={general.textInputArea}
              placeholder='Mensagem'
              placeholderTextColor={colors.borderInput}
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType={'next'}
              underlineColorAndroid="transparent"
              value={this.state.Mensagem}
              onChangeText={Mensagem => this.setState({ Mensagem })}
              />
          </View>

          <View style={{  justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 15  }}>
            <Buttons
              title='ENVIAR'
              icon='arrow-right'
              width={'95%'}
              gradient={true}
              _onPress={() => this.handleContato()}
            />
          </View>
        </View>
        </KeyboardAwareScrollView>

        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.openPicker1}
          >
          <View style={styles.containerPicker}>
          <View style={styles.closeButtonContainer}>
            <TouchableHighlight onPress={ () => {this.setState({
              openPicker1: false
            })} } underlayColor="transparent" style={styles.closeButton}>
              <Text style={styles.closeButtonText}>Choose</Text>
            </TouchableHighlight>
          </View>
          <PickerIOS
            selectedValue={this.state.Assunto}
            itemStyle={{
              height: 200,
              width: '100%'
            }}
            onValueChange={(itemValue, itemIndex) => {this.changeAssunto(itemValue)}}>
            <PickerIOS.Item
              value='Informações'
              label='Informações'
            />
            <PickerIOS.Item
              value='Sugestões'
              label='Sugestões'
            />
            <PickerIOS.Item
              value='Dúvidas'
              label='Dúvidas'
            />
            <PickerIOS.Item
              value='Agradecimentos'
              label='Agradecimentos'
            />
            <PickerIOS.Item
              value='Reclamações'
              label='Reclamações'
            />
            <PickerIOS.Item
              value='Outros'
              label='Outros'
            />

          </PickerIOS>
          </View>
        </Modal>

      </View>
    )
  }
}

const mapStateToProps = state => ({
  faleconosco: state.faleconosco,
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...FaleConoscoActions,
    ...UserActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ContatoPage);
