import { StyleSheet, Dimensions } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: metrics.basePadding*2
    },
    boxSocial: {
      width: '100%',
      marginTop: metrics.baseMargin,
      backgroundColor: '#f2f2f2',
      padding: metrics.basePadding,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    buttonSocial: {
      width: 40,
      height: 40,
      borderRadius: 100,
      justifyContent: 'center',
      alignItems: 'center',
      marginRight: 10,
      backgroundColor: colors.purple,
    },
    txtSocial: {
      fontFamily: fonts.r700,
      color: colors.purple,
      marginRight: 10,
    },
    logo: {
        width: 150,
        height: 89,
        resizeMode: 'contain',
        marginTop: 30,
        marginBottom: 20,
    },
    containerBox: {
        position: 'relative',
        backgroundColor: colors.white,
        paddingVertical: metrics.basePadding*2,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        width: '100%',
    },
    h1: {
       fontFamily: fonts.r700,
       color: colors.purple,
       fontSize: 18,
       textAlign: 'center',
       width: '100%'
    },
    h2: {
       fontFamily: fonts.r700,
       color: colors.purple,
       fontSize: 18,
       textAlign: 'center',
       width: '100%',
    },
    BoxEsqueciSenha: {
        marginTop: 30
    },
    BoxButton: {
        position: 'relative',
        top: 40
    },
    butCadastrar: {
        fontFamily: fonts.r700,
        color: colors.white,
        fontSize: 16,
        width: '100%',
        marginTop: 40,
        textAlign: 'center'
    }
});

export default styles;
