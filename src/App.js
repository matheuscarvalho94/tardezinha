import React from "react";
import { Provider } from "react-redux";
import { NetInfo } from "react-native";
// import PushNotification from 'react-native-push-notification';
NetInfo.isConnected.addEventListener("connectionChange", console.log);

import "./config/reactotron";
import store from "./store";

import AppNavigator from './routes/routes'

import { isSignedIn } from "./services/auth";

export let navigatorRef;

export default class App extends React.Component {
  state = {
    signed: false,
    signLoaded: false,
  };

  componentDidMount(){
    navigatorRef = this.navigatorRef;
  }

  componentWillMount() {
    // navigatorRef = this.navigatorRef;

    // isSignedIn()
    //   .then(res => this.setState({ signed: res, signLoaded: true }))
    //   .catch(err => alert("Erro"));
  }

  render() {
    return (
      <Provider store={store}>
        <AppNavigator
          ref={nav => { this.navigatorRef = nav; } }
          />
      </Provider>
    )
  }
}
