import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { colors, metrics, fonts } from '../styles'
const ListArrecadacao = ({
  data
}) => (
    <View style={styles.row}>
        <Text style={styles.h2}>ListArrecadacao</Text>
        <Text style={styles.value}>108</Text>
    </View>
);

const styles = StyleSheet.create({
  row: {
    borderBottomWidth: 1,
    borderColor: colors.divisor,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  h2: {
    fontFamily: fonts.r400,
    color: colors.subTxt2,
    fontSize: 15
  },
  value: {
    fontFamily: fonts.r700,
    color: colors.pink,
    fontSize: 15
  }
})

export default ListArrecadacao;
