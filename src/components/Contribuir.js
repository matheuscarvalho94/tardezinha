import React from 'react';
import { Text, View, TouchableOpacity, TextInput , StyleSheet, Platform} from 'react-native';
import { fonts, colors, general } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const Contribuir = ({
  value
}) => (
  <View>
    <Text>NÚMERO DE CONTRIBUIÇÕES</Text>
    <View style={general.flexRow}>
      <TextInput
        style={styles.textinput}
        placeholder='Digite seu cep'
        placeholderTextColor={colors.borderInput}
        autoCapitalize="none"
        autoCorrect={false}
        returnKeyType={'next'}
        underlineColorAndroid="transparent"
        value={this.state.value}
        onChangeText={value => this.setState({ value })}
      />

      <TouchableOpacity style={styles.buttonCount}>
        <Icon name='plus' size={20} color={colors.white} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonCount}>
        <Icon name='plus' size={20} color={colors.white} />
      </TouchableOpacity>

    </View>
  </View>
);

const styles = StyleSheet.create({
    buttonCount: {
      height: 35,
      width: 35,
      padding: 2,
      backgroundColor: colors.pink,
      borderRadius: 100,
      marginRight: 10,
      marginTop: 10,
    },
    textinput: {
      height: 49,
      paddingVertical: Platform.OS==='android'? 0: 10,
      marginBottom: 10,
      width: '80%',
      backgroundColor: 'transparent',
      borderBottomColor: colors.pink,
      borderBottomWidth: 3,
      fontSize: 16,
      color: colors.borderInput,
      fontFamily: fonts.r400,
    },
    txt: {
        fontFamily: fonts.r700,
        color: colors.purple,
        fontSize: 16
    }
});


export default Contribuir;
