import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { fonts, colors, metrics } from '../styles';
// import TrackPlayer from 'react-native-track-player';

const ListSongs = ({
    data,
    active
}) => (
    <View style={styles.row}>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={styles.button}>
          <Icon name={active ?'pause' :'play'} size={15} color={active ?colors.pink :colors.purple} style={{ marginLeft: 5}} />
        </TouchableOpacity>
        <View style={styles.colInfo}>
          <Text style={[styles.song, { color: active ?colors.pink :colors.purple }]}>{data.Nome}</Text>
          <Text style={styles.artist}>{data.Autor}</Text>
        </View>
      </View>
      <View>
        <Text style={styles.time}>{data.Minutos}</Text>
      </View>
    </View>
);

const styles = StyleSheet.create({
  row:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 35,
  },
  button: {
    width: 35,
    height: 35,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: colors.purple,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
  colInfo: {
    flexDirection: 'column',
  },
  song: {
    fontFamily: fonts.r700,
    fontSize: 18,
  },
  artist: {
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 15,
  },
  time: {
    fontSize: 14,
    fontFamily: fonts.r400,
    color: colors.purple
  }
})

export default ListSongs;
