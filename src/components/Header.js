import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, Platform, AsyncStorage, StatusBar } from 'react-native';
import { fonts, colors, metrics } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as UserActions } from '../store/ducks/user';

class Header extends React.Component {

  componentDidMount = async () => {
    const { redux } = this.props;
    if(redux){
      let token = await AsyncStorage.getItem('token')
      this.props.getUserRequest(token)
    }
  }

  render() {
    const {
      user,
      title,
      background,
      nocadastro,
      goback,
      navigation
    } = this.props;
    return (
      <View>
        {
          goback
          ?<LinearGradient colors={background} style={[styles.ButtonView]} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
            <StatusBar backgroundColor='#EF9427' barStyle="light-content" />
            <View style={{ flexDirection: 'row', alignItems: 'center',}}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon name='arrow-left' size={15} color={colors.white} />
              </TouchableOpacity>
              <Text style={[styles.txt, {marginLeft: 15}]} onPress={() => navigation.goBack()}>{title}</Text>
            </View>
          </LinearGradient>
          :<LinearGradient colors={background} style={[styles.ButtonView]} start={{x: 0.0, y: 0.0}} end={{x: 0.9, y: 0.0}}>
              {
                nocadastro
                ?<View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%' }}>
                    <Text style={styles.txt}>{title}</Text>
                    <LinearGradient style={{ width: 55, height: 55, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'relative', top: 0, zIndex: 999}} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
                     { user.data.Foto
                       ?<TouchableOpacity style={{ width: 50, height: 50,overflow: 'hidden',  borderRadius: 100, backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center' }}  onPress={() => {
                        navigation.navigate('EditarPerfilPage')
                      }}>
                        <Image source={{uri: user.data.Foto}} style={{height: '100%', width: '100%'}}/>
                      </TouchableOpacity>
                      :<TouchableOpacity style={{ width: 50, height: 50, borderRadius: 100, backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center' }} onPress={() => {
                        navigation.navigate('EditarPerfilPage')
                      }} >
                        <Icon name='user' size={45} color={colors.purple} />
                      </TouchableOpacity>}
                    </LinearGradient>
                  </View>
                </View>
                :<Text onPress={() => navigation.goBack()} style={styles.txt}>{title}</Text>
              }
          </LinearGradient>

        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  ButtonView: {
    width: '100%',
    height: 100,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#000',
    paddingHorizontal: metrics.basePadding*2,
    paddingTop: Platform.OS==='android' ?0 :15,
  },
  txt: {
    fontFamily: fonts.r700,
    color: colors.white,
    fontSize: 25
  }
});

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
