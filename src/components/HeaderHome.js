import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Platform, AsyncStorage, Image } from 'react-native';
import { fonts, colors, metrics } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as UserActions } from '../store/ducks/user';

class HeaderHome extends React.Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token')
    this.props.getUserRequest(token)
  }

  render() {
    const { user, navigation } = this.props;
    return (
      <View style={[styles.ButtonView]}>
        <TouchableOpacity style={styles.photo} onPress={() => {
          navigation.navigate('EditarPerfilPage')
        }}>
          <LinearGradient style={{width: 55, height: 55, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'relative', top: 0, zIndex: 999}} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
            {user.data.Foto
            ?<View style={{ width: 50, height: 50, overflow: 'hidden', borderRadius: 100, backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={{uri: user.data.Foto}} style={{height: '100%', width: '100%'}}/>
            </View>
            :<View style={{ width: 50, height: 50, borderRadius: 100, backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center' }}>
              <Icon name='user' size={45} color={colors.purple} />
            </View>
            }
          </LinearGradient>
        </TouchableOpacity>
        <View style={styles.infoPerfil}>
          <TouchableOpacity onPress={() => {
            navigation.navigate('EditarPerfilPage')
          }}>
            <Text style={styles.h1} >Olá {user.data.Nome} </Text>
          </TouchableOpacity>
          <Text style={styles.h2}>Fique por dentro do que está acontecendo</Text>
        </View>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  infoPerfil: {
    flexDirection: 'column'
  },
  ButtonView: {
    padding: metrics.basePadding*2,
    width: '100%',
    marginRight: 'auto',
    marginLeft: 'auto',
    height: 110,
    flexDirection: 'row',
    paddingTop: Platform.OS==='android' ?20 :30,
  },
  photo: {
    width: 55,
    height: 55,
    marginRight: 15,
    backgroundColor: '#000',
    borderRadius: 100,
  },
  h1: {
    fontFamily: fonts.r700,
    color: colors.pink,
    fontSize: 25,
    margin: 0,
  },
  h2: {
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 14,
    margin: 0,
    width: '90%'
  }
});


const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HeaderHome);
