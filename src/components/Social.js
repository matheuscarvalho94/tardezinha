import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { colors, metrics, fonts } from '../styles';

const SocialComponent = ({
    navigation,
    name,
    backgroundButton,
    _onPress,
}) => (
  <View>
    {
      backgroundButton
      ?<TouchableOpacity style={styles.backgroundButton} onPress={_onPress} >
        <Icon size={20} color={colors.white} name={name}/>
      </TouchableOpacity>
      :<TouchableOpacity style={styles.nobackground} onPress={_onPress} >
        <Icon size={Platform.OS==='android'?20:15} color={colors.orange} name={name}/>
        <Text style={styles.small}>Sair</Text>
      </TouchableOpacity>
    }
  </View>
);

const styles = StyleSheet.create({
  backgroundButton: {
    borderRadius: 10,
    backgroundColor: colors.orange,
    width: Platform.OS==='android'?50:40,
    height: Platform.OS==='android'?50:40,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  nobackground: {
    borderRadius: 10,
    borderColor: colors.orange,
    borderWidth: 1,
    width: Platform.OS==='android'?50:40,
    height: Platform.OS==='android'?50:40,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: metrics.basePadding
  },
  small: {
    fontFamily: fonts.r400,
    color: colors.orange,
    fontSize: Platform.OS==='android'?13:10,
  }
})

export default SocialComponent;
