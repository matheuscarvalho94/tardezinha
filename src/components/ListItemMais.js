import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { colors, fonts, metrics } from '../styles';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const ListItemMais = ({
  navigation
}) => (
  <View style={styles.container}>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('AturnePage')}}>
        <Text style={styles.h1}>A TURNÊ DO TARDEZINHA</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('SobrePage')}}>
        <Text style={styles.h1}>SOBRE O TARDEZINHA DO BEM</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('ArrecadacaoPage')}}>
        <Text style={styles.h1}>ARRECADAÇÃO</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('ParceirosPage')}}>
        <Text style={styles.h1}>PARCEIROS</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('MusicasPage')}}>
        <Text style={styles.h1}>MÚSICAS EXCLUSIVAS</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('VoucherPage')}}>
        <Text style={styles.h1}>VOUCHER</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
    <TouchableOpacity style={styles.row} onPress={() => { navigation.navigate('AjudaPage')}}>
        <Text style={styles.h1}>AJUDA</Text>
        <Icon size={13} name='chevron-right' color={colors.subTxt1} />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    paddingVertical: metrics.basePadding,
    flexDirection: 'column',
  },
  row: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  h1: {
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 15,
  },
})

export default ListItemMais;
