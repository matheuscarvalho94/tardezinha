import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, } from 'react-native';
import { colors } from '../styles';


const ListParceiros = ({
  data,
  open
}) => (
  <View style={styles.row}>
  {
    data.map((data, index) => {
      return (
        <TouchableOpacity style={styles.item} key={index} onPress={() => open(data.URL)}>
          <View style={[styles.backgroundImage]}>
            <Image
              style={styles.imagem}
              source={{uri: data.Imagem}}
              />
          </View>
        </TouchableOpacity>
      )
    })
  }
  </View>
);

const styles = StyleSheet.create({
  row: {
    marginTop: 20,
    width: '95%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginRight: 'auto',
    marginLeft: 'auto',
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  item: {
    width: '48%',
    borderRadius: 10,
    borderColor: colors.pink,
    borderWidth:1,
    overflow: 'hidden',
    marginBottom: 10,
    position: 'relative'
  },
  backgroundImage:{
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagem: {
    width: '100%',
    height: 100,
    resizeMode: 'contain'
  },
})

export default ListParceiros;
