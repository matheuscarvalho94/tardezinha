import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Platform, Image, Linking } from 'react-native';
// import Video from 'react-native-video';
import { fonts, colors, metrics } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import HTML from 'react-native-render-html';
const truncate = require('truncate-html')
const DEFAULT_PROPS = {
  onLinkPress: (evt, href) => { Linking.openURL(href); },
  debug: true
};
class Posts extends React.Component {

  state={
    lines: 80,
    changeChamada: true
  }

  onLinkPress (evt, href, htmlAttribs) {
    alert(`Opened ${href} ! Attributes: ${JSON.stringify(htmlAttribs)}`);
  }

  openFulltext = () => {
  this.state.changeChamada==true ? this.setState({changeChamada:false}) : this.setState({changeChamada:true})
  }

  render() {
    const {
      data,
    } = this.props;

    return (
      <View style={{
        borderRadius: 10,
        borderColor: 'rgba(238,238,238, 0.7)',
        borderWidth:1,
        overflow: 'hidden',
        marginBottom: 10,
        position: 'relative'
      }}>
        <LinearGradient style={styles.photo} colors={['#BFCA65', '#8FC8A8', '#62C5E9']}>
          <View style={{ width: '85%', height: '85%', borderRadius: 100, overflow: 'hidden', }}>
            <Image source={{ uri: data.Imagem2 }} style={{ width: '100%', height: '100%' }} />
          </View>
        </LinearGradient>
        <View style={styles.box_header}>

          <LinearGradient colors={['#62C5E9', '#8FC8A8', '#BFCA65']} style={[styles.header]} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
            <View style={styles.infoPost}>
              <Text style={styles.h1}>{data.Subtitulo}</Text>
            </View>
          </LinearGradient>
        </View>
        {
          data.Imagem==null
          ?<Image source={{ uri: data.Imagem }} style={styles.imagem}/>
          :<Image source={{ uri: data.Imagem }} style={styles.imagem}/>
        }

        <View style={styles.container}>
          <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
            <Text style={styles.h3}>{data.Titulo}</Text>
            <Text style={[styles.h3, { fontSize: 14 }]}>{data.DataHoraFormatada}</Text>
          </View>
          <TouchableOpacity  onPress={() => this.openFulltext()}>
          {
            this.state.changeChamada
            ?<HTML html={ truncate(this.props.data.Chamada) }  />
            :<HTML html={ truncate(this.props.data.Texto) } {...DEFAULT_PROPS} />
          }
            <Image source={require('../assets/icons/iconreticencia.png')} style={{  width: 32, height: 17, resizeMode: 'contain', marginLeft: 'auto', marginRight: 'auto',  }} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  box_header: {
    position: 'relative'
  },
  header: {
    padding: metrics.basePadding*2,
    width: '100%',
    height: 35,
    padding: 0,
    flexDirection: 'row',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
  },
  infoPost: {
    flexDirection: 'row',
  },
  photo: {
    width: 50,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#000',
    borderRadius: 100,
    padding: 0,
    zIndex: 9,
    position: 'absolute',
    top: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  h1: {
    fontFamily: fonts.r700,
    color: colors.white,
    fontSize: 18,
    marginLeft: Platform.OS==='android'?80:80
  },
  h2: {
    fontFamily: fonts.r400,
    color: colors.white,
    fontSize: 13,
    margin: 4,
  },
  container: {
    width: '100%',
    padding: metrics.basePadding*2
  },
  h3: {
    fontFamily: fonts.r700,
    color: colors.facebook,
    fontSize: 18,
  },
  h4: {
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 14,
  },
  imagem: {
    width: '100%',
    height: 350,
    position: 'relative',
    zIndex: 0
  }
});

export default Posts;
