import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import { fonts, colors } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const Buttons = ({
    title,
    width,
    gradient,
    background,
    _onPress,
    icon,
    navigation,
}) => (
  <View>
      {
        gradient
        ?<TouchableOpacity onPress={ _onPress}>
            <LinearGradient colors={['#7D6AAE', '#AD60A4', '#E45398']} style={[styles.ButtonView, { width: width }]} start={{x: 0.0, y: 0.0}} end={{x: 0.9, y: 0.0}}>
                <Text style={styles.txt}>{title}</Text>
                {
                  icon
                  ?<Icon name={icon} size={22} color={colors.white} style={{marginLeft: 10}}/>
                  :null
                }
            </LinearGradient>
        </TouchableOpacity>
        :<TouchableOpacity onPress={_onPress} style={[styles.ButtonView, { width: width, backgroundColor: background }]}>
            <Text style={styles.txt}>{title}</Text>
        </TouchableOpacity>
      }

  </View>
);

const styles = StyleSheet.create({
    ButtonView: {
        height: 49,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#000',
        borderRadius: 30,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    txt: {
        fontFamily: fonts.r700,
        color: colors.white,
        fontSize: 16
    }
});


export default Buttons;
