import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import { fonts, colors, metrics } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const Box = ({
    width,
    message,
    _onPress,
}) => (
  <View>
    <TouchableOpacity onPress={ _onPress} style={{ position: 'relative', width: '100%', justifyContent: 'center' }}>
    <View style={{ width: '100%',position: 'relative'}}>
      <Image source={require('../assets/icons/5.png')} style={{ height: 12, resizeMode: 'contain', position: 'relative', top: 10, left: 0, right: 0, marginRight: 'auto', marginLeft: 'auto' }} />
    </View>
    <LinearGradient colors={['#7D6AAE', '#AD60A4', '#E45398']} style={[styles.ButtonView, { width: width }]} start={{x: 0.0, y: 0.0}} end={{x: 0.9, y: 0.0}}>
        <Text style={styles.txt}>{message}</Text>
      </LinearGradient>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
    ButtonView: {
        marginTop: 10,
        position: 'relative',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#000',
        borderRadius: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        padding: metrics.basePadding,
    },
    txt: {
        fontFamily: fonts.r400,
        color: colors.white,
        fontSize: 16,
        width: '100%',
        textAlign: 'center'
    }
});


export default Box;
