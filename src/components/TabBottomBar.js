import React from 'react';
import { View,Text, StyleSheet, TouchableWithoutFeedback, Platform, Dimensions } from 'react-native';
import { colors, fonts } from '../styles/index'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { NavigationActions, StackActions } from 'react-navigation';
import { navigatorRef } from "../App";
import {BoxShadow} from 'react-native-shadow'
const {width} = Dimensions.get('window');

const shadowOpt = {
  width: width,
  ...ifIphoneX({
    height: 85,
    paddingBottom: 20
  }, {
    height: 70
  }),
  color:"#000",
  border:1,
  radius:10,
  opacity:0.1,
  x:0,
  y:-2,
}

const styles = StyleSheet.create({
  tabbar: {
    ...ifIphoneX({
      height: 85,
      paddingBottom: 20
    }, {
      height: 70
    }),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: '#000',
    borderWidth: 0,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    backgroundColor: colors.white,
  },
  tab: {
    alignSelf: 'stretch',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5
  },
  textBar: {
    fontSize: 10,
    fontFamily: fonts.r700,
    color: colors.borderInput,
    marginTop: 5
  }
});

class TabBar extends React.Component {

  constructor(props) {
      super(props);
  }

  jump(key){
    const nav = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'MainRoute',
          params: {},
          action: NavigationActions.navigate({ routeName: key, params: {} }),
        }),
      ],
      key: null
    });


    navigatorRef.dispatch(nav);

  }

  render() {
    //console.log(this.props)
    const {
      navigation,
      renderIcon,
      getLabelText,
      activeTintColor,
      inactiveTintColor,
      jumpTo
    } = this.props;

    const {routes} = navigation.state;

    return (
      <BoxShadow setting={shadowOpt}>
      <View style={styles.tabbar}>
        {
            routes && routes.map((route, index) => {
            const focused = index === navigation.state.index;
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            return (
              <TouchableWithoutFeedback
                key={route.key}
                onPressIn={() => route.key == "Menulateral" ? navigation.openDrawer() : this.jump(route.key)  }
                style={styles.tab}
              >
                <View style={styles.tab}>
                  {
                    renderIcon({
                    route,
                    index,
                    focused,
                    tintColor
                  })}
                  {
                    focused
                    ?<Text style={[styles.textBar, {color: '#E73458'}]}>{getLabelText({route: route})}</Text>
                    :<Text style={styles.textBar}>{getLabelText({route: route})}</Text>
                  }

                </View>
              </TouchableWithoutFeedback>
            );
          })}

      </View>
    </BoxShadow>
    );
  }
};

export default TabBar;
