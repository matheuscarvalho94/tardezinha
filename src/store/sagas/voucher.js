import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as VoucherActions } from '../ducks/voucher';

export function* getVoucher(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.get, `/Vouchers`, headers);
    yield put(VoucherActions.getVoucherSuccess(response.data));
  }catch (err){
    yield put(VoucherActions.getVoucherFailure(err));
  }
}
