import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as ContribuirActions } from '../ducks/contribuir';

export function* getContribuir(action) {
  const { Usuario } = action.payload;
  const token = Usuario.token.replace('"', '').replace('"', '')
  console.log(Usuario);
  const params = {
    quantidade: Usuario.count,
    ProductId: Usuario.ProductId,
    Token: Usuario.TokenProduct,
    Plataforma: Usuario.Platform,
  };

  const headers = {
    headers: {
      appid: 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.post, `/Eventos/Resgatar/`+Usuario.Id, params, headers);
    yield put(ContribuirActions.getContribuirSuccess(response.data));
  }catch (err){
    yield put(ContribuirActions.getContribuirFailure(err));
  }
}
