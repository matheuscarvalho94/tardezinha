import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as ArrecadacaoActions } from '../ducks/arrecadacao';

export function* getArrecadacao(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.get, `/Paginas/4`, headers);
    yield put(ArrecadacaoActions.getArrecadacaoSuccess(response.data));
  }catch (err){
    yield put(ArrecadacaoActions.getArrecadacaoFailure(err));
  }
}
