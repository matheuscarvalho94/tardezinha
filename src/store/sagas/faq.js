import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as FaqActions } from '../ducks/faq';

export function* getFaq(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.get, `/Faq`, headers);
    yield put(FaqActions.getFaqSuccess(response.data));
  }catch (err){
    yield put(FaqActions.getFaqFailure(err));
  }
}
