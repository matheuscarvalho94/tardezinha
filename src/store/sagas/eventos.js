import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as EventosActions } from '../ducks/eventos';

export function* getEventos(action) {
  const { Usuario } = action.payload;

  const token = Usuario.token.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.get, `/Eventos?page=1&size=`+Usuario.quantidade, headers);
    yield put(EventosActions.getEventosSuccess(response.data));
  }catch (err){
    yield put(EventosActions.getEventosFailure(err));
  }
}
