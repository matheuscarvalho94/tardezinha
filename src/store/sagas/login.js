import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import queryString from 'query-string';
import { AsyncStorage } from 'react-native';
import { navigatorRef } from '../../App';
import { Creators as LoginActions } from '../ducks/login';

import { NavigationActions,StackActions } from 'react-navigation';

export function* getLogin(action) {
  const { Usuario } = action.payload;

  const nav = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: 'MainRoute'
      })
    ],
    key: null
  });



  const headers = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  };

  try{
    if(Usuario.facebook==true){
      const paramsfb = queryString.stringify({
        grant_type: 'password',
        username: Usuario.emailfb,
        password: Usuario.passwordfb,
        appid: 'diretoria-do-bem',
      });
      const response = yield call(api.post, `/token`, paramsfb, headers);
       yield put(LoginActions.getLoginSuccess(response.data));
       const storeAndNavigate = async () => {
        await AsyncStorage.setItem('token', JSON.stringify(response.data.access_token));
        navigatorRef.dispatch(nav);
      };
      setTimeout(storeAndNavigate,0)
    }else if(Usuario.google==true){
      const paramsfb = queryString.stringify({
        grant_type: 'password',
        username: Usuario.emailfb,
        password: Usuario.passwordfb,
        appid: 'diretoria-do-bem',
      });
      const response = yield call(api.post, `/token`, paramsfb, headers);
       yield put(LoginActions.getLoginSuccess(response.data));
       const storeAndNavigate = async () => {
        await AsyncStorage.setItem('token', JSON.stringify(response.data.access_token));
        navigatorRef.dispatch(nav);
      };
      setTimeout(storeAndNavigate,0)
    }else if(Usuario.Cadastro==true){
      const paramsfb = queryString.stringify({
        grant_type: 'password',
        username: Usuario.Email,
        password: Usuario.Senha,
        appid: 'diretoria-do-bem',
      });
      const response = yield call(api.post, `/token`, paramsfb, headers);
      yield put(LoginActions.getLoginSuccess(response.data));

      const storeAndNavigate = async () => {
        await AsyncStorage.setItem('token', JSON.stringify(response.data.access_token));
      };
      setTimeout(storeAndNavigate,0)

    }else{
      const params = queryString.stringify({
        grant_type: 'password',
        username: Usuario.email,
        password: Usuario.password,
        appid: 'diretoria-do-bem',
      });
      const response = yield call(api.post, `/token`, params, headers);
      yield put(LoginActions.getLoginSuccess(response.data));
      const storeAndNavigate = async () => {
        await AsyncStorage.setItem('token', JSON.stringify(response.data.access_token));
        navigatorRef.dispatch(nav);
      };
      setTimeout(storeAndNavigate,0)
  }
  }catch (err){
    yield put(LoginActions.getLoginFailure(err));
  }
}
