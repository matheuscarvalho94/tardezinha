import { call, put } from 'redux-saga/effects';
import { Platform } from 'react-native';
import api from '../../services/api';
import { Creators as VersaoActions } from '../ducks/versao';
import { navigatorRef } from '../../App';

import { NavigationActions } from 'react-navigation';
import config from '../../config/config';

export function* getVersao(action) {

  const nav = NavigationActions.navigate({
    routeName: 'VersaoPage'
  })

  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')
  const headers = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer '+token
    },
  };
  try {
    const response = yield call(api.get, '/Usuarios/RetornaVersao', headers);
    yield put(VersaoActions.getVersaoSuccess(response.data));
    //console.log(response.data, 'DATAA')
    if(response.data.VersaoAndroid!=config.Version_Android && Platform.OS == 'android'){
        navigatorRef.dispatch(nav);
    }else if (response.data.VersaoiOs != config.Version_IOS && Platform.OS == 'ios'){
        navigatorRef.dispatch(nav);
    }
  } catch (err) {
  yield put(VersaoActions.getVersaoFailure(err));
  //console.log(err)
  }
  }

