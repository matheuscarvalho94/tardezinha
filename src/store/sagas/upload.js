import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as UploadActions } from '../ducks/upload';
import config from "../../config/config";

export function* getUpload(action) {
  const { Usuario } = action.payload;
  // const token = Usuario.token.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'Content-Type': undefined,
      'appId': config.appId
      // 'Authorization': 'bearer '+token
    },
  };

  const data = new FormData();
  data.append('name', 'testName'); // you can append anyone.
  console.log(Usuario, 'Usuario')
  data.append('photo', {
    uri: 'file://'+Usuario.Foto,
    type: 'image/jpeg', // or photo.type
    name: Usuario.namephoto
  });


  let params = {
    body: data,
  }

  try {
    const response = yield call(api.post, `/Usuarios/UploadFoto`, params.body, headers);
    yield put(UploadActions.getUploadSuccess(response.data));
  } catch (err) {
    yield put(UploadActions.getUploadFailure(err));
  }
}

