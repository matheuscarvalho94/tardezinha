import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import queryString from 'query-string';

import { Creators as FaleConoscoActions } from '../ducks/faleconosco';

export function* getFaleConosco(action) {
  const { Usuario } = action.payload;
  const token = Usuario.token.replace('"', '').replace('"', '')

  const params = {
    Nome: Usuario.Nome,
    Origem: 2,
    Email: Usuario.Email,
    Estado: Usuario.Estado,
    Cidade: Usuario.Cidade,
    CPF: Usuario.CPF,
    Empresa: Usuario.Empresa,
    Assunto: Usuario.Assunto,
    Mensagem: Usuario.Mensagem,

  };

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.post, `/FaleConosco/Cadastrar`, params, headers);
    yield put(FaleConoscoActions.getFaleConoscoSuccess(response.data));
  }catch (err){
    yield put(FaleConoscoActions.getFaleConoscoFailure(err));
  }
}
