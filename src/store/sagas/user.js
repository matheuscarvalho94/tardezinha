import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as UserActions } from '../ducks/user';
import { navigatorRef } from '../../App';
import { NavigationActions,StackActions } from 'react-navigation';

export function* getUser(action) {
  const { Usuario } = action.payload;

  const token = Usuario? Usuario.replace('"', '').replace('"', '') : null
  const nav = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: 'LoginPage'
      })
    ],
    key: null
  });

  const headers = {
    headers: {
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.get, `/Usuarios`, headers);
    yield put(UserActions.getUserSuccess(response.data));
  }catch (err){
    yield put(UserActions.getUserFailure(err));
    const storeAndNavigate = async () => {
      navigatorRef.dispatch(nav);
    };
    setTimeout(storeAndNavigate,0)


  }
}
