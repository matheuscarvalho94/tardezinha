import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as AgendaActions } from '../ducks/agenda';

export function* getAgenda(action) {
  const { Usuario } = action.payload;

  const token = Usuario.token.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.get, `/Conteudos/2?quantidade=${Usuario.quantidade}`, headers);
    yield put(AgendaActions.getAgendaSuccess(response.data));
  }catch (err){
    yield put(AgendaActions.getAgendaFailure(err));
  }
}
