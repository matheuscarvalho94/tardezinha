import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import queryString from 'query-string';
import { Creators as EditarPerfilActions } from '../ducks/editarperfil';

export function* getEditarPerfil(action) {
  const { Usuario } = action.payload;
  let cpfconvert = Usuario.CPF.replace(/[^\d]+/g,'')
  const cepconvert = Usuario.Cep.replace("-", "");
  const token = Usuario.token.replace('"', '').replace('"', '')

  const params = {
    Foto: Usuario.Foto,
    Nome: Usuario.Nome,
    Sobrenome: Usuario.Sobrenome,
    Email: Usuario.Email,
    CPF: cpfconvert,
    DataNascimento: Usuario.ano + '-' + Usuario.mes + '-' + Usuario.dia,
    Celular: Usuario.Celular,
    Senha: Usuario.Senha,
    ConfirmarSenha: Usuario.ConfirmarSenha,
    EnderecoLogradouro: Usuario.Endereco,
    EnderecoCidade: Usuario.Cidade,
    EnderecoCEP: cepconvert,
    ConfirmarSenha: Usuario.ConfirmarSenha,
    IdFacebook: Usuario.IdFacebook,
  };

  const headers = {
    headers: {
      appid: 'diretoria-do-bem',
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + token,
    },
  };

  try{
    const response = yield call(api.post, `/Usuarios/Edit`, params, headers);
    yield put(EditarPerfilActions.getEditarPerfilSuccess(response.data));
  }catch (err){
    yield put(EditarPerfilActions.getEditarPerfilFailure(err));
  }
}
