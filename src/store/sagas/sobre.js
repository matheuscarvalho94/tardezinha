import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as SobreActions } from '../ducks/sobre';

export function* getSobre(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.get, `/Paginas/5`, headers);
    yield put(SobreActions.getSobreSuccess(response.data));
  }catch (err){
    yield put(SobreActions.getSobreFailure(err));
  }
}
