import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as FazeroBemActions } from '../ducks/fazerobem';

export function* getFazeroBem(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.get, `/Conteudos/7`, headers);
    yield put(FazeroBemActions.getFazeroBemSuccess(response.data));
  }catch (err){
    yield put(FazeroBemActions.getFazeroBemFailure(err));
  }
}
