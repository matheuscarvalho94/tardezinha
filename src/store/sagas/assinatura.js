import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Platform } from 'react-native';
import queryString from 'query-string';
import { Creators as AssinaturaActions } from '../ducks/assinatura';

export function* getAssinatura(action) {
  const { Usuario } = action.payload;

  const params = {
    AppId: 'diretoria-do-bem',
    Email: Usuario.email,
    Senha: Usuario.password,
    ProductId: Platform.OS==='android'?Usuario.GoogleSubscriptionId:Usuario.AppStoreSubscriptionId,
    Token: Platform.OS==='android'?Usuario.GooglePurchaseToken:Usuario.AppStorePurchaseToken,
    Plataforma: Platform.OS==='android'?'android':'ios',
  };

  const headers = {
    headers: {
      appid: 'diretoria-do-bem',
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.post, `/Usuarios/ReativarAssinatura`, params, headers);
    yield put(AssinaturaActions.getAssinaturaSuccess(response.data));
  }catch (err){
    yield put(AssinaturaActions.getAssinaturaFailure(err));
  }
}
