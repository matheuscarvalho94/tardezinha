import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'arrecadacao/GET_REQUEST',
  GET_SUCCESS: 'arrecadacao/GET_SUCCESS',
  GET_FAILURE: 'arrecadacao/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function arrecadacao(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getArrecadacaoRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getArrecadacaoSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getArrecadacaoFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
