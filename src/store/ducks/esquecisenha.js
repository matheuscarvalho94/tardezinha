import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'esquecisenha/GET_REQUEST',
  GET_SUCCESS: 'esquecisenha/GET_SUCCESS',
  GET_FAILURE: 'esquecisenha/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  alert: false,
  message: '',
  sucess: false,
  icon: '',
  error:false,
  error_message:''
});


export default function esquecisenha(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        sucess: true,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, sucess: false, loading: false, alert: true, error:true, error_message: action.payload.response.data.Message, icon: action.payload.icon };

    default:
      return state;
  }
}


export const Creators = {

  getEsqueciSenhaRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),


  getEsqueciSenhaSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getEsqueciSenhaFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
