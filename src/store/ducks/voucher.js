import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'voucher/GET_REQUEST',
  GET_SUCCESS: 'voucher/GET_SUCCESS',
  GET_FAILURE: 'voucher/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function voucher(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getVoucherRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getVoucherSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getVoucherFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
