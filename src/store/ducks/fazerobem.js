import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'fazerobem/GET_REQUEST',
  GET_SUCCESS: 'fazerobem/GET_SUCCESS',
  GET_FAILURE: 'fazerobem/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function fazerobem(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getFazeroBemRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getFazeroBemSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getFazeroBemFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
