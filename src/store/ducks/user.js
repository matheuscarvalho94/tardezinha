import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'user/GET_REQUEST',
  GET_SUCCESS: 'user/GET_SUCCESS',
  GET_FAILURE: 'user/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
});


export default function user(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, alert: true, error:true, error_message: action.payload.error_message, icon: action.payload.icon };

    default:
      return state;
  }
}


export const Creators = {

  getUserRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),


  getUserSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getUserFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
