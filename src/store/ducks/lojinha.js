import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'lojinha/GET_REQUEST',
  GET_SUCCESS: 'lojinha/GET_SUCCESS',
  GET_FAILURE: 'lojinha/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function lojinha(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getLojinhaRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getLojinhaSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getLojinhaFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
