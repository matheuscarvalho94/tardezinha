import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'upload/GET_REQUEST',
  GET_SUCCESS: 'upload/GET_SUCCESS',
  GET_FAILURE: 'upload/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  alert: false,
  message: '',
  icon: '',
  error:false,
  error_message:''
});


export default function upload(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: false };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, alert: true, error:true, error_message: action.payload.response.data.Message, icon: action.payload.icon };

    default:
      return state;
  }
}


export const Creators = {

  getUploadRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),


  getUploadSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getUploadFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
