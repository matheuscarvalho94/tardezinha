import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'cadastro/GET_REQUEST',
  GET_SUCCESS: 'cadastro/GET_SUCCESS',
  GET_FAILURE: 'cadastro/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  alert: false,
  message: '',
  icon: '',
  error:false,
  error_message:''
});


export default function cadastro(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return {
        ...state,
        error: true,
        loading: false,
        error_message: action.payload.response.data.Message,
       };

    default:
      return state;
  }
}


export const Creators = {

  getCadastroRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),


  getCadastroSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getCadastroFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
