import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'banners/GET_REQUEST',
  GET_SUCCESS: 'banners/GET_SUCCESS',
  GET_FAILURE: 'banners/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function banners(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getBannersRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getBannersSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getBannersFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
