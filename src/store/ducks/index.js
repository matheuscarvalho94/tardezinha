import { combineReducers } from "redux";

import { reducer as offline } from "redux-offline-queue";
import versao from "./versao";
import login from "./login";
import cadastro from "./cadastro";
import upload from "./upload";
import facebook from "./facebook";
import google from "./google";
import user from "./user";
import eventos from "./eventos";
import timeline from "./timeline";
import agenda from "./agenda";
import fazerobem from "./fazerobem";
import lojinha from "./lojinha";
import aturne from "./aturne";
import sobre from "./sobre";
import arrecadacao from "./arrecadacao";
import parceiros from "./parceiros";
import voucher from "./voucher";
import faq from "./faq";
import faleconosco from "./faleconosco";
import esquecisenha from "./esquecisenha";
import editarperfil from "./editarperfil";
import paginas from "./paginas";
import contribuir from "./contribuir";
import assinatura from "./assinatura";

export default combineReducers({
  offline,
  versao,
  login,
  cadastro,
  upload,
  facebook,
  google,
  user,
  eventos,
  timeline,
  agenda,
  fazerobem,
  lojinha,
  aturne,
  sobre,
  arrecadacao,
  parceiros,
  voucher,
  faq,
  faleconosco,
  esquecisenha,
  editarperfil,
  paginas,
  contribuir,
  assinatura
});
