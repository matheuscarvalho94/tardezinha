import {
  createStackNavigator,
} from 'react-navigation';

import MaisPage from '../screens/mais/Mais'
import AturnePage from '../screens/aturne/Aturne'
import AgendaPage from '../screens/agenda/Agenda'
import SobrePage from '../screens/sobre/Sobre'
import ArrecadacaoPage from '../screens/arrecadacao/Arrecadacao'
import MusicasPage from '../screens/musicas/Musicas'
import VoucherPage from '../screens/voucher/Voucher'

export default MaisStackNavigator = createStackNavigator({
  MaisPage: { screen: MaisPage },
  AturnePage: { screen: AturnePage },
  AgendaPage: { screen: AgendaPage },
  SobrePage: { screen: SobrePage },
  ArrecadacaoPage: { screen: ArrecadacaoPage},
  MusicasPage: { screen: MusicasPage },
  VoucherPage: { screen: VoucherPage },
}, {
  headerMode: "none",
  navigationOptions: {
    gesturesEnabled: false,
    tabBarVisible: false,
}

});
