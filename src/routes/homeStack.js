import {
  createStackNavigator,
} from 'react-navigation';

import HomePage from '../screens/home/Home'

export default HomeStackNavigator = createStackNavigator({
  HomePage: { screen: HomePage },
}, {
  headerMode: "none",
});
