import {
  createStackNavigator,
} from 'react-navigation';

import AgendaPage from '../screens/agenda/Agenda'
import EventoPage from '../screens/evento/Evento'

export default AgendaStackNavigator = createStackNavigator({
  AgendaPage: { screen: AgendaPage },
}, {
  headerMode: "none",
});
