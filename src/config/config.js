const URL = 'https://www.am4cms.com.br/api';

export const appId = {
  name: 'diretoria-do-bem'
}

export const config = {
  URL,
  nameApp: 'Tardezinha',
  Version_Android: '1.0',
  Version_IOS: '1.0',
  imagemPerfil: URL+'/Conteudo/Clientes/'+appId.name+'/associados/',
  imagemLojas: URL+'/conteudo/clientes/'+appId.name+'/Lojas/',
  imagemNotificacoes: URL+'/conteudo/clientes/'+appId.name+'/Mensagens/',
  imagemBeneficio: URL+'/conteudo/clientes/'+appId.name+'/beneficios/',
  imagemBanners: URL+'/conteudo/clientes/'+appId.name+'/banners/',
  imagemMedalha: URL+'/conteudo/clientes/'+appId.name+'/categoriaspontos/',
};

export default config;
